﻿(function ($, window, document, undefined) {
    STS.Portal.vm.RecruitmentViewModel = kendo.observable({
        // #region properties
        IsInListMode: false,
        IsInEditMode: false,
        consultantId: null,
        preferredLocationId: null,
        isUniversityNotAvailable: false,
        isDegreeNotAvailable: false,
        isPrimarySkillNotAvailable: false,
        isSecondarySkillNotAvailable: false,
        education: [],
        preferredLocation: [],
        Id: null,
        prefLocId: null,
        // #endregion

        consultantCollection: new STS.Portal.DataModels.RecruitementDataModel(),
        preferredLoacationCollection: new STS.Portal.DataModels.PrefferedRelocationDataModel(),
        educationCollection: new STS.Portal.DataModels.EducationDataModel(),
        degreeCollection: new STS.Portal.DataModels.DegreeModel(),
        documentCollection: new STS.Portal.DataModels.DocumentsDataModel(),

        WorkAuthorizationTypeDs: new kendo.data.DataSource({ data: [] }),
        ConsultantDs: new kendo.data.DataSource({ data: [], pageSize: 10 }),
        CountryDs: new kendo.data.DataSource({ data: [] }),
        StatesDs: new kendo.data.DataSource({ data: [] }),
        CityDs: new kendo.data.DataSource({ data: [] }),
        PrefCountryDs: new kendo.data.DataSource({ data: [] }),
        PrefStatesDs: new kendo.data.DataSource({ data: [] }),
        PrefCityDs: new kendo.data.DataSource({ data: [] }),
        PrefferedLocationDs: new kendo.data.DataSource({ data: [], pageSize: 10 }),
        universityDs: new kendo.data.DataSource({ data: [] }),
        degreeDs: new kendo.data.DataSource({ data: [] }),
        educationDs: new kendo.data.DataSource({ data: [], pageSize: 10 }),
        primarySkillsDs: new kendo.data.DataSource({ data: [] }),
        secondarySkillsDs: new kendo.data.DataSource({ data: [] }),
        sallaryBillingModeDs: new kendo.data.DataSource({
            data: [
                { text: "hourly", value: "hourly" },
                { text: "annually", value: "annually" }
            ]
        }),
        idTypeDs: new kendo.data.DataSource({ data: [] }),
        photoIdTypeDs: new kendo.data.DataSource({ data: [] }),
        documentDs: new kendo.data.DataSource({ data: [], pageSize: 10 }),
        statusDs: new kendo.data.DataSource({ data: [] }),

        Load: function (e) {
            var that = this;
            STS.Portal.vm.dashboardModel.set("moduleName", "Recruitment");
            STS.Portal.vm.dashboardModel.loadview();

            STS.Portal.DataSources.GetAllConsultants(function (result) {
                that.ConsultantDs.data(result);
            });
        },
        addNewCounsultant: function (e) {
            var that = this;
            that.set("consultantId", null);
            that.set("consultantCollection", new STS.Portal.DataModels.RecruitementDataModel());
            location.href = "#/Add";
        },
        OnClickDeleteConsultant: function (e) {
            var that = this;
            STS.Portal.DataSources.DeleteConsultant(e.data.ConsultantId, function (result) {
                that.ConsultantDs.data(result);
            });
        },
        showAll: function (e) {
            var that = this;
            that.set("consultantId", null);
            that.set("consultantCollection", new STS.Portal.DataModels.RecruitementDataModel());
            location.href = "#/";

            STS.Portal.DataSources.GetAllConsultants(function (result) {
                that.ConsultantDs.data(result);
            });
        },
        LoadTabStrip: function () {
            var that = this;
            STS.Portal.DataSources.getAllAuthTypes(function (result) {
                that.WorkAuthorizationTypeDs.data(result);
            });
            //STS.Portal.DataSources.getAllCountires(function (result) {
            //    that.CountryDs.data(result);
            //});
            STS.Portal.DataSources.GetAllSkills(function (result) {
                that.primarySkillsDs.data(result.primarySkills);
                that.secondarySkillsDs.data(result.secondarySkills);
            });
            STS.Portal.DataSources.GetAllStatus(function (result) {
                that.statusDs.data(result);
            });

            $("#tabstrip").kendoTabStrip({
                animation: {
                    open: {
                        effects: "fadeIn"
                    }
                }
            });

            var ts = $("#tabstrip").data("kendoTabStrip");
            if (that.consultantId == null) {
                ts.enable(ts.tabGroup.children("li:eq(1)"), false);
                ts.enable(ts.tabGroup.children("li:eq(2)"), false);
                ts.enable(ts.tabGroup.children("li:eq(3)"), false);
                ts.enable(ts.tabGroup.children("li:eq(4)"), false);
                ts.enable(ts.tabGroup.children("li:eq(5)"), false);
            } else {
                ts.enable(ts.tabGroup.children("li:eq(0)"), true);
                ts.enable(ts.tabGroup.children("li:eq(1)"), true);
                ts.enable(ts.tabGroup.children("li:eq(2)"), true);
                ts.enable(ts.tabGroup.children("li:eq(3)"), true);
                ts.enable(ts.tabGroup.children("li:eq(4)"), true);
                ts.enable(ts.tabGroup.children("li:eq(5)"), true);

                $.ajax({
                    type: 'GET',
                    url: 'https://battuta.medunes.net/api/country/all/?key=cf525291deea29d98f5be3f0bcb27257',
                    dataType: 'json',
                    contentType: 'json',
                    headers: { 'api-key': 'myKey' },
                    success: function (countries) {
                        that.CountryDs.data(countries);
                    },
                    error: function (e) {
                        if (e.statusText == "error") {
                        }
                    }
                });

                //$.get('https://battuta.medunes.net/api/country/all/?key=cf525291deea29d98f5be3f0bcb27257', function (countries) {
                //    that.CountryDs.data(countries);
                //});

                STS.Portal.DataSources.LoadConsultantDetail(that.consultantId, function (data) {
                    that.set("consultantCollection", data);
                    var cntryDropDown = $("#countryDropdownList").data("kendoDropDownList");
                    if (cntryDropDown) {
                        var cntryDropdownData = cntryDropDown.dataSource._data;
                        for (var i = 0; i < cntryDropdownData.length; i++) {
                            if (cntryDropdownData[i].name == data.CurrentLocationCountryName) {
                                that.consultantCollection.set("CurrentLocationCountry", cntryDropdownData[i].code);
                                break;
                            }
                        }

                        if (that.consultantCollection.CurrentLocationCountry) {
                            url = 'https://battuta.medunes.net/api/region/' + that.consultantCollection.CurrentLocationCountry + '/all/?key=cf525291deea29d98f5be3f0bcb27257';
                            $.get(url, '', function (data) {
                                that.StatesDs.data(data);
                            });
                        }

                        if (that.consultantCollection.CurrentLocationCountry && that.consultantCollection.CurrentLocationState) {
                            url = 'https://battuta.medunes.net/api/city/' + that.consultantCollection.CurrentLocationCountry + '/search/?region=' + that.consultantCollection.CurrentLocationState + '&key=cf525291deea29d98f5be3f0bcb27257';

                            $.get(url, '', function (data) {
                                that.CityDs.data(data);
                            });
                        }
                    }

                });

                STS.Portal.DataSources.GetAllPreferredLocationByConsulantId(that.consultantId, function (result) {
                    that.PrefferedLocationDs.data(that.preferredLocation);
                    //that.PrefferedLocationDs.data(result);
                });

                STS.Portal.DataSources.GetAllEducationDetailsByConsultantId(that.consultantId, function (result) {
                    if (result != null && that.education.length == 0) {
                        for (var i = 0; i < result.length; i++) {
                            var startDate = new Date(result[i].StartDate);
                            var endDate = new Date(result[i].EndDate);

                            var object = {
                                Id: that.education.length + 1,
                                University_Mapping_Id: result[i].University_Mapping_Id,
                                University_Id: result[i].University_Id,
                                University_Name: result[i].University_Name,
                                Degree_Id: result[i].Degree_Id,
                                Degree_Name: result[i].Degree_Name,
                                Consultant_Id: result[i].Consultant_Id,
                                StartDate: startDate,
                                EndDate: endDate
                            }
                            that.education.push(object);
                        }
                    }
                });

                that.educationDs.data(that.education);
                that.PrefferedLocationDs.data(that.preferredLocation);
                STS.Portal.DataSources.GetAllDocumentsByConsultantId(that.consultantId, function (result) {
                    for (var i = 0; i < result.length; i++) {
                        result[i].Valid_From = new Date(result[i].Valid_From);
                        result[i].Valid_To = new Date(result[i].Valid_To);
                    }
                    that.documentDs.data(result);
                });
            }
        },
        OnClickEditConsultant: function (e) {

        },
        onCountryChange: function (e) {
            var that = this;
            that.StatesDs.data([]);
            that.CityDs.data([]);

            var cntryName = $("#countryDropdownList").data("kendoDropDownList").text();
            var cntryCode = $("#countryDropdownList").data("kendoDropDownList").value();
            that.consultantCollection.set("CurrentLocationCountryName", cntryName);

            if (cntryCode) {
                url = 'https://battuta.medunes.net/api/region/' + cntryCode + '/all/?key=cf525291deea29d98f5be3f0bcb27257';
                $.get(url, '', function (data) {
                    that.StatesDs.data(data);
                });
            }

            //    STS.Portal.DataSources.GetAllStates(that.consultantCollection.CurrentLocationCountryId, function (result) {
            //        that.StatesDs.data(result);
            //    });
            //}
        },
        onStateChange: function (e) {
            var that = this;
            that.CityDs.data([]);

            if (that.consultantCollection.CurrentLocationCountry && that.consultantCollection.CurrentLocationState) {
                url = 'https://battuta.medunes.net/api/city/' + that.consultantCollection.CurrentLocationCountry + '/search/?region=' + that.consultantCollection.CurrentLocationState + '&key=cf525291deea29d98f5be3f0bcb27257';

                $.get(url, '', function (data) {
                    that.CityDs.data(data);
                });
            }

            //that.consultantCollection.set("CurrentLocationState", $("#statesDropdownList").data("kendoDropDownList").text());

            //if (that.consultantCollection.CurrentLocationStateId != null) {
            //    STS.Portal.DataSources.GetAllCities(that.consultantCollection.CurrentLocationStateId, function (result) {
            //        that.CityDs.data(result);
            //    });
            //}
        },
        onCityChange: function (e) {
            var that = this;
            //that.consultantCollection.set("CurrentLocationCity", $("#cityDropdownList").data("kendoDropDownList").text());
        },
        tab1Save: function (e) {
            var that = this;
            if (that.validateTab1()) {
                if (that.consultantId == null) {
                    STS.Portal.DataSources.CreateConsultant(that.consultantCollection, function (data) {
                        if (data.Statuscode == 200) {
                            that.set("consultantCollection", data.consultant);
                            console.log(data.consultant);
                            that.set("consultantId", data.consultant.ConsultantId);
                            toastr.success(data.Status);
                            that.enableAllTabs();
                        } else {
                            toastr.error(data.Status);
                        }
                    });
                } else {
                    that.consultantCollection.set("Context", "PersonalInfo");
                    STS.Portal.DataSources.UpdateConsultant(that.consultantCollection, function (data) {
                        if (data.Statuscode == 200) {
                            that.set("consultantCollection", data.Consultant);
                            toastr.success(data.Status);
                            that.enableAllTabs();
                        } else {
                            toastr.error(data.Status);
                        }
                    });
                }
            }
        },
        tab1Cancel: function () {
            var that = this;
            if (that.consultantId == null) {
                that.set("consultantCollection", new STS.Portal.DataModels.RecruitementDataModel());
            } else {
                STS.Portal.DataSources.LoadConsultantDetail(that.consultantId, function (data) {
                    that.set("consultantCollection", data);
                });
            }
        },
        tab1Next: function () {
            var that = this;
            if (that.validateTab1()) {
                if (that.consultantId == null) {
                    STS.Portal.DataSources.CreateConsultant(that.consultantCollection, function (data) {
                        if (data.Statuscode == 200) {
                            that.set("consultantId", data.Cons_Id);
                            toastr.success(data.Status);
                            that.enableAllTabs();

                            $("#tabstrip-1").css("height", "100%");
                            var tabToActivate = $("#tab2li");
                            $("#tabstrip").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);
                        } else {
                            toastr.error(data.Status);
                        }
                    });
                } else {
                    that.consultantCollection.set("Context", "PersonalInfo");
                    STS.Portal.DataSources.UpdateConsultant(that.consultantCollection, function (data) {
                        if (data.Statuscode == 200) {
                            that.set("consultantCollection", data.Consultant);
                            toastr.success(data.Status);
                            that.enableAllTabs();

                            $("#tabstrip-1").css("height", "100%");
                            var tabToActivate = $("#tab2li");
                            $("#tabstrip").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);
                        } else {
                            toastr.error(data.Status);
                        }
                    });
                }
            }
        },
        tab2Previous: function () {
            //var that = this;
            //if (that.validateTab1()) {
            //    if (that.consultantId == null) {
            //        STS.Portal.DataSources.CreateConsultant(that.consultantCollection, function (data) {
            //            if (data.Statuscode == 200) {
            //                that.set("consultantId", data.Cons_Id);
            //                toastr.success(data.Status);
            //                that.enableAllTabs();

            //                $("#tabstrip-2").css("height", "100%");
            //                var tabToActivate = $("#tab1li");
            //                $("#tabstrip").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);
            //            } else {
            //                toastr.error(data.Status);
            //            }
            //        });
            //    } else {
            //        that.consultantCollection.set("Context", "PersonalInfo");
            //        STS.Portal.DataSources.UpdateConsultant(that.consultantCollection, function (data) {
            //            if (data.Statuscode == 200) {
            //                that.set("consultantCollection", data.Consultant);
            //                toastr.success(data.Status);
            //                that.enableAllTabs();

            //                $("#tabstrip-2").css("height", "100%");
            //                var tabToActivate = $("#tab1li");
            //                $("#tabstrip").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);
            //            } else {
            //                toastr.error(data.Status);
            //            }
            //        });
            //    }
            //}
        },
        validateTab1: function () {
            var that = this;
            if (!that.consultantCollection.FName || !that.consultantCollection.LName || !that.consultantCollection.Email || !that.consultantCollection.PhoneNumber || !that.consultantCollection.WorkAuthorizationTypeId || !that.consultantCollection.DoeUsa) {
                toastr.error("Please provide all the details!");
                return false;
            }

            var x = that.consultantCollection.Email;
            var atpos = x.indexOf("@");
            var dotpos = x.lastIndexOf(".");
            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                toastr.error("Not a valid e-mail address!");
                return false;
            }

            if ((that.consultantCollection.PhoneNumber).toString().length != 10) {
                toastr.error("Your Mobile Number must be 1 to 10 Integers!");
                return false;
            }

            var today = new Date();
            var doe = that.consultantCollection.DoeUsa;
            if (doe >= today) {
                toastr.error("Date of entr in USA should be less than today's date!");
                return false;
            }

            var re = /^[A-Za-z]+$/;
            if (!re.test(that.consultantCollection.FName) || !re.test(that.consultantCollection.LName)) {
                toastr.error("Name should not contain numbers!");
                return false;
            }

            return true;
        },
        enableAllTabs: function () {
            var ts = $("#tabstrip").data("kendoTabStrip");
            console.log(ts);
            ts.enable(ts.tabGroup.children("li:eq(0)"), true);
            ts.enable(ts.tabGroup.children("li:eq(1)"), true);
            ts.enable(ts.tabGroup.children("li:eq(2)"), true);
            ts.enable(ts.tabGroup.children("li:eq(3)"), true);
            ts.enable(ts.tabGroup.children("li:eq(4)"), true);
            ts.enable(ts.tabGroup.children("li:eq(5)"), true);
        },
        addNewPrefferedLocation: function () {
            var that = this;
            location.href = "#/AddPrefferedLocation/" + that.consultantId;
            that.set("preferredLoacationCollection", new STS.Portal.DataModels.PrefferedRelocationDataModel());
        },
        LoadAllDropdowns: function () {
            var that = this;
            $.get('https://battuta.medunes.net/api/country/all/?key=cf525291deea29d98f5be3f0bcb27257', '', function (data) {
                that.PrefCountryDs.data(data);
            });
            //STS.Portal.DataSources.getAllCountires(function (result) {
            //    that.PrefCountryDs.data(result);
            //});
        },
        onPreCountryChange: function () {
            var that = this;
            that.PrefStatesDs.data([]);
            that.PrefCityDs.data([]);

            var cntryName = $("#countryDropdownList-pref").data("kendoDropDownList").text();
            var cntryCode = $("#countryDropdownList-pref").data("kendoDropDownList").value();
            that.preferredLoacationCollection.set("PreferredRelocationCountryName", cntryName);

            if (cntryCode) {
                url = 'https://battuta.medunes.net/api/region/' + cntryCode + '/all/?key=cf525291deea29d98f5be3f0bcb27257';
                $.get(url, '', function (data) {
                    that.PrefStatesDs.data(data);
                });
            }

            //that.preferredLoacationCollection.set("Preferred_Relocation_Country", $("#countryDropdownList-pref").data("kendoDropDownList").text());
            //if (that.preferredLoacationCollection.Preferred_Relocation_Country_Id != null) {
            //    STS.Portal.DataSources.GetAllStates(that.preferredLoacationCollection.Preferred_Relocation_Country_Id, function (result) {
            //        that.PrefStatesDs.data(result);
            //    });
            //}
        },
        onPreStateChange: function () {
            var that = this;
            that.PrefCityDs.data([]);

            if (that.preferredLoacationCollection.PreferredRelocationCountry && that.preferredLoacationCollection.PreferredRelocationState) {
                url = 'https://battuta.medunes.net/api/city/' + that.preferredLoacationCollection.PreferredRelocationCountry + '/search/?region=' + that.preferredLoacationCollection.PreferredRelocationState + '&key=cf525291deea29d98f5be3f0bcb27257';

                $.get(url, '', function (data) {
                    that.PrefCityDs.data(data);
                });
            }

            //that.preferredLoacationCollection.set("Preferred_Relocation_State", $("#statesDropdownList-pref").data("kendoDropDownList").text());
            //if (that.preferredLoacationCollection.Preferred_Relocation_State_Id != null) {
            //    STS.Portal.DataSources.GetAllCities(that.preferredLoacationCollection.Preferred_Relocation_State_Id, function (result) {
            //        that.PrefCityDs.data(result);
            //    });
            //}
        },
        onPreCityChange: function () {
            var that = this;
            //that.preferredLoacationCollection.set("Preferred_Relocation_City", $("#cityDropdownList-pref").data("kendoDropDownList").text());
        },
        savePrefferedLocation: function () {
            var that = this;
            that.preferredLoacationCollection.set("ConsultantId", that.consultantId);
            if (!that.preferredLoacationCollection.PreferredRelocationCity &&
                !that.preferredLoacationCollection.PreferredRelocationCountry &&
                !that.preferredLoacationCollection.PreferredRelocationCountryName &&
                !that.preferredLoacationCollection.PreferredRelocationState &&
                !that.preferredLoacationCollection.PreferredRelocationZipCode) {
                toastr.error("Please provide data before save!");
                return false;
            }

            if (that.prefLocId == null) {
                that.preferredLoacationCollection.prefLocId = that.preferredLocation.length + 1;
                that.preferredLocation.push(that.preferredLoacationCollection);
            }
            that.set("preferredLoacationCollection", new STS.Portal.DataModels.PrefferedRelocationDataModel());
            location.href = "#/Edit/" + that.consultantId;

            //if (that.preferredLoacationCollection.Preferred_Relocation_City_Id == 0 ||
            //    that.preferredLoacationCollection.Preferred_Relocation_City_Id == null) that.preferredLoacationCollection.Preferred_Relocation_City = "";
            //if (that.preferredLoacationCollection.Preferred_Relocation_Country_Id == 0 ||
            //    that.preferredLoacationCollection.Preferred_Relocation_Country_Id == null) that.preferredLoacationCollection.Preferred_Relocation_Country = "";
            //if (that.preferredLoacationCollection.Preferred_Relocation_State_Id == 0 ||
            //    that.preferredLoacationCollection.Preferred_Relocation_State_Id == null) that.preferredLoacationCollection.Preferred_Relocation_State = "";

            //STS.Portal.DataSources.SavePreferredLocation(that.preferredLoacationCollection, function (data) {
            //    if (data.Statuscode == 200) {
            //        that.set("preferredLocationId", data.PreferredRelocation_Id);
            //        toastr.success(data.Status);
            //        that.set("preferredLoacationCollection", new STS.Portal.DataModels.PrefferedRelocationDataModel());
            //        location.href = "#/Edit/" + that.consultantId;

            //    } else {
            //        toastr.error(data.Status);
            //    }
            //});
        },
        cancelPrefferedLocation: function () {
            var that = this;
            that.set("preferredLoacationCollection", new STS.Portal.DataModels.PrefferedRelocationDataModel());
            location.href = "#/Edit/" + that.consultantId;
        },
        EditPreferredLoc: function (consultantId, prefLocId) {
            var that = this;
            var object = null;
            for (var i = 0; i < that.preferredLocation.length; i++) {
                if (that.preferredLocation[i].prefLocId == prefLocId) {
                    object = that.preferredLocation[i];
                    that.set("preferredLoacationCollection", that.preferredLocation[i]);
                }
            }

            that.set("preferredLoacationCollection", object);
            $.get('https://battuta.medunes.net/api/country/all/?key=cf525291deea29d98f5be3f0bcb27257', function (countries) {
                that.PrefCountryDs.data(countries);
            });

            if (that.preferredLoacationCollection.PreferredRelocationCountry) {
                url = 'https://battuta.medunes.net/api/region/' + that.preferredLoacationCollection.PreferredRelocationCountry + '/all/?key=cf525291deea29d98f5be3f0bcb27257';
                $.get(url, '', function (data) {
                    that.PrefStatesDs.data(data);
                });
            }

            if (that.preferredLoacationCollection.PreferredRelocationCountry && that.preferredLoacationCollection.PreferredRelocationState) {
                url = 'https://battuta.medunes.net/api/city/' + that.preferredLoacationCollection.PreferredRelocationCountry + '/search/?region=' + that.preferredLoacationCollection.PreferredRelocationState + '&key=cf525291deea29d98f5be3f0bcb27257';

                $.get(url, '', function (data) {
                    that.PrefCityDs.data(data);
                });
            }

            $("#statesDropdownList-pref").data("kendoDropDownList").value(that.preferredLoacationCollection.PreferredRelocationState);
            $("#cityDropdownList-pref").data("kendoDropDownList").value(that.preferredLoacationCollection.PreferredRelocationCity);

            //STS.Portal.DataSources.GetPreferredLocForEdit(preferredRelocationId, function (result) {
            //    that.LoadAllDropdowns();
            //    that.set("preferredLoacationCollection", result);
            //    var country = $("#countryDropdownList-pref").data("kendoDropDownList");
            //    if (country != null) country.value(result.Preferred_Relocation_Country_Id);

            //    STS.Portal.DataSources.GetAllStates(0, function (states) {
            //        that.PrefStatesDs.data(states);
            //        var state = $("#statesDropdownList-pref").data("kendoDropDownList");
            //        if (state != null) state.value(result.Preferred_Relocation_State_Id);
            //    });
            //    STS.Portal.DataSources.GetAllCities(0, function (cities) {
            //        that.PrefCityDs.data(cities);
            //        var city = $("#cityDropdownList-pref").data("kendoDropDownList");
            //        if (city != null) city.value(result.Preferred_Relocation_City_Id);
            //    });
            //})
        },
        OnClickDeletePreferredLocation: function (e) {
            var that = this;
            STS.Portal.DataSources.DeletePreferredLocationByConsulantId(e.data, function (data) {
                toastr.success(data);
                STS.Portal.DataSources.GetAllPreferredLocationByConsulantId(that.consultantId, function (result) {
                    console.log(result);
                    that.PrefferedLocationDs.data(result);
                });
            });
        },
        tab2Save: function (e) {
            var that = this;
            if (that.validateTab2()) {
                that.consultantCollection.set("Context", "ContactInformation");
                that.updateContactInformation(that.consultantCollection);
            }
        },
        validateTab2: function () {
            var that = this;
            console.log(that.consultantCollection);
            if (!that.consultantCollection.CurrentLocationCountryName || !that.consultantCollection.CurrentLocationStreet || !that.consultantCollection.CurrentLocationZipcode) {
                toastr.error("Please provide all the required details!");
                return false;
            }
            return true;
        },
        updateContactInformation: function (consultantCollection) {
            var that = this;
            STS.Portal.DataSources.UpdateConsultant(consultantCollection, function (data) {
                if (data.Statuscode == 200) {
                    console.log(data);
                    that.set("consultantCollection", data.Consultant);
                    toastr.success(data.Status);
                } else {
                    toastr.error(data.Status);
                }
            });
        },
        tab2Cancel: function () {
            var that = this;
            that.tab1Cancel();
            var data = that.consultantCollection;
            if (data.CurrentLocationCountryId != 0 || data.CurrentLocationCountryId != null) {
                STS.Portal.DataSources.GetAllStates(that.consultantCollection.CurrentLocationCountryId, function (result) {
                    that.StatesDs.data(result);
                    if (data.CurrentLocationStateId != 0 || data.CurrentLocationStateId != null) {
                        STS.Portal.DataSources.GetAllCities(that.consultantCollection.CurrentLocationStateId, function (result) {
                            that.CityDs.data(result);
                            that.set("consultantCollection", data);
                        });
                    }
                });
            }
        },
        tab2Next: function () {
            var that = this;
            if (that.validateTab2()) {
                that.consultantCollection.set("Context", "ContactInformation");
                that.updateContactInformation(that.consultantCollection);

                var tabToActivate = $("#tab3li");
                $("#tabstrip").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);
            }
        },
        addNewEducation: function () {
            var that = this;
            location.href = "#/addNewEducation/" + that.consultantId;
            that.set("preferredLoacationCollection", new STS.Portal.DataModels.PrefferedRelocationDataModel());
        },
        LoadEducationData: function () {
            var that = this;

            STS.Portal.DataSources.GetAllUniversities(function (result) {
                that.universityDs.data(result);
            });

            STS.Portal.DataSources.GetAllDegrees(function (result) {
                that.degreeDs.data(result);
            });
        },
        onChangeUniversityDropdown: function (e) {
            var that = this;
            var combobox = $("#universityComboBox").data("kendoComboBox").text();
            var data = that.universityDs._data;

            if (combobox == "") {
                that.set("isUniversityNotAvailable", false);
                return false;
            }
            if (data.length == 0) {
                that.set("isDegreeNotAvailable", true);
                return false;
            }

            var uniName = [];
            for (var i = 0; i < data.length; i++) {
                uniName.push(data[i].University_Name);
            }

            if (uniName.indexOf(combobox) > -1) {
                that.set("isUniversityNotAvailable", false);
            } else {
                that.set("isUniversityNotAvailable", true);
            }
        },
        addNewUniversity: function (e) {
            var that = this;
            if (that.isUniversityNotAvailable == true) {
                var combobox = $("#universityComboBox").data("kendoComboBox").text();

                STS.Portal.DataSources.AddUniversity(combobox, function (result) {
                    that.universityDs.data(result);
                    that.set("isUniversityNotAvailable", false);
                });
            }
        },
        onChangeDegreeDropdown: function (e) {
            var that = this;
            var combobox = $("#degreeComboBox").data("kendoComboBox").text();
            var data = that.degreeDs._data;

            if (combobox == "") {
                that.set("isDegreeNotAvailable", false);
                return false;
            }
            if (data.length == 0) {
                that.set("isDegreeNotAvailable", true);
                return false;
            }

            var degreeName = [];
            for (var i = 0; i < data.length; i++) {
                degreeName.push(data[i].Degree_name);
            }

            if (degreeName.indexOf(combobox) > -1) {
                that.set("isDegreeNotAvailable", false);
            } else {
                that.set("isDegreeNotAvailable", true);
            }
        },
        addNewDegree: function (e) {
            var that = this;
            if (that.isDegreeNotAvailable == true) {
                var combobox = $("#degreeComboBox").data("kendoComboBox").text();

                STS.Portal.DataSources.AddDegree(combobox, function (result) {
                    that.degreeDs.data(result);
                    that.set("isDegreeNotAvailable", false);
                });
            }
        },
        addEducation: function (e) {
            var that = this;

            if (that.educationCollection.University_Id == 0 && that.degreeCollection.Degree_Id == 0 &&
                that.educationCollection.StartDate == null && that.educationCollection.EndDate == null) {
                toastr.error("Please provide data before save!");
                return false;
            }

            if (that.educationCollection.StartDate < that.educationCollection.EndDate) {
                if (that.Id == null) {
                    var object = {
                        Id: that.education.length + 1,
                        University_Mapping_Id: 0,
                        University_Id: that.educationCollection.University_Id,
                        University_Name: $("#universityComboBox").data("kendoComboBox").text(),
                        Degree_Id: that.degreeCollection.Degree_Id,
                        Degree_Name: $("#degreeComboBox").data("kendoComboBox").text(),
                        Consultant_Id: that.consultantId,
                        StartDate: that.educationCollection.StartDate,
                        EndDate: that.educationCollection.EndDate
                    }
                    that.education.push(object);
                } else {
                    var object = {
                        Id: that.Id,
                        University_Mapping_Id: 0,
                        University_Id: that.educationCollection.University_Id,
                        University_Name: $("#universityComboBox").data("kendoComboBox").text(),
                        Degree_Id: that.degreeCollection.Degree_Id,
                        Degree_Name: $("#degreeComboBox").data("kendoComboBox").text(),
                        Consultant_Id: that.consultantId,
                        StartDate: that.educationCollection.StartDate,
                        EndDate: that.educationCollection.EndDate
                    }
                    for (var i = 0; i < that.education.length; i++) {
                        if (that.education[i].Id == that.Id) {
                            that.education[i] = object;
                        }
                    }
                }

                that.set("educationCollection", new STS.Portal.DataModels.EducationDataModel());
                that.set("degreeCollection", new STS.Portal.DataModels.DegreeModel());
                location.href = "#/Edit/" + that.consultantId;

            } else {
                toastr.error("End date should be greater than start date!");
                return false;
            }
        },
        cancelEducation: function (e) {
            var that = this;
            that.set("educationCollection", new STS.Portal.DataModels.EducationDataModel());
            that.set("degreeCollection", new STS.Portal.DataModels.DegreeModel());
            location.href = "#/Edit/" + that.consultantId;
        },
        editEducation: function (consultantId, id) {
            var that = this;
            that.LoadEducationData();
            for (var i = 0; i < that.education.length; i++) {
                if (that.education[i].Id === id) {
                    that.educationCollection.set("University_Id", that.education[i].University_Id);
                    that.educationCollection.set("University_Name", that.education[i].University_Name);
                    that.degreeCollection.set("Degree_Id", that.education[i].Degree_Id);
                    that.degreeCollection.set("Degree_Name", that.education[i].Degree_Name);
                    that.educationCollection.set("StartDate", that.education[i].StartDate);
                    that.educationCollection.set("EndDate", that.education[i].EndDate);
                    that.set("consultantId", that.education[i].Consultant_Id);
                    that.set("Id", that.education[i].Id);
                }
            }
        },
        onClickDeleteEducation: function (e) {
            var that = this;
            for (var i = 0; i < that.education.length; i++) {
                if (that.education[i].Id == e.data.Id) {
                    that.education[i].IsDeleted = true;
                }
            }

            var temp = [];
            for (var i = 0; i < that.education.length; i++) {
                if (!that.education[i].hasOwnProperty('IsDeleted') || that.education[i].IsDeleted == false) {
                    temp.push(that.education[i]);
                }
            }

            that.education = [];
            that.education = temp;
            that.educationDs.data(that.education);
        },
        tab3Save: function (e) {
            var that = this;
            that.saveEducation();
        },
        tab3Next: function (e) {
            var that = this;
            that.saveEducation();
            var tabToActivate = $("#tab4li");
            $("#tabstrip").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);
        },
        tab3Cancel: function () {
            var that = this;
            that.education = [];

            STS.Portal.DataSources.GetAllEducationDetailsByConsultantId(that.consultantId, function (result) {
                if (result != null && that.education.length == 0) {
                    for (var i = 0; i < result.length; i++) {
                        var startDate = new Date(result[i].StartDate);
                        var endDate = new Date(result[i].EndDate);

                        var object = {
                            Id: that.education.length + 1,
                            University_Mapping_Id: result[i].University_Mapping_Id,
                            University_Id: result[i].University_Id,
                            University_Name: result[i].University_Name,
                            Degree_Id: result[i].Degree_Id,
                            Degree_Name: result[i].Degree_Name,
                            Consultant_Id: result[i].Consultant_Id,
                            StartDate: startDate,
                            EndDate: endDate
                        }
                        that.education.push(object);
                    }
                }
                that.educationDs.data(that.education);
            });
        },
        tab3Previous: function () {
            //var that = this;
            //that.saveEducation();
            //var tabToActivate = $("#tab2li");
            //$("#tabstrip").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);
        },
        saveEducation: function (e) {
            var that = this;
            if (that.education.length > 0) {
                STS.Portal.DataSources.SaveEducationsList(that.education, function () {
                    that.education = [];
                    STS.Portal.DataSources.GetAllEducationDetailsByConsultantId(that.consultantId, function (result) {
                        if (result != null && that.education.length == 0) {
                            for (var i = 0; i < result.length; i++) {
                                var startDate = new Date(result[i].StartDate);
                                var endDate = new Date(result[i].EndDate);

                                var object = {
                                    Id: that.education.length + 1,
                                    University_Mapping_Id: result[i].University_Mapping_Id,
                                    University_Id: result[i].University_Id,
                                    University_Name: result[i].University_Name,
                                    Degree_Id: result[i].Degree_Id,
                                    Degree_Name: result[i].Degree_Name,
                                    Consultant_Id: result[i].Consultant_Id,
                                    StartDate: startDate,
                                    EndDate: endDate
                                }
                                that.education.push(object);
                            }
                        }
                        that.educationDs.data(that.education);
                        toastr.success("Education details updated successfully!");
                    });
                });
            } else {
                toastr.error("Provide atlease one education detail before saving!");
                return false;
            }
        },
        onChangePrimarySkill: function (e) {
            var that = this;
            var multiselect = $("#primarySkillDropDown").data("kendoMultiSelect").text();
            var data = that.primarySkillsDs._data;

            if (multiselect == "") {
                that.set("isPrimarySkillNotAvailable", false);
                return false;
            }
            if (data.length == 0) {
                that.set("isPrimarySkillNotAvailable", true);
                return false;
            }

            var temp = [];
            for (var i = 0; i < data.length; i++) {
                temp.push(data[i].Skill_Name);
            }

            if (temp.indexOf(multiselect) > -1) {
                that.set("isPrimarySkillNotAvailable", false);
            } else {
                that.set("isPrimarySkillNotAvailable", true);
            }
        },
        onChangeSecondarySkill: function (e) {
            var that = this;
            var multiselect = $("#secondarySkillDropDown").data("kendoMultiSelect").text();
            var data = that.secondarySkillsDs._data;

            if (multiselect == "") {
                that.set("isSecondarySkillNotAvailable", false);
                return false;
            }
            if (data.length == 0) {
                that.set("isSecondarySkillNotAvailable", true);
                return false;
            }

            var temp = [];
            for (var i = 0; i < data.length; i++) {
                temp.push(data[i].Skill_Name);
            }

            if (temp.indexOf(multiselect) > -1) {
                that.set("isSecondarySkillNotAvailable", false);
            } else {
                that.set("isSecondarySkillNotAvailable", true);
            }
        },
        tab4Save: function (e) {
            var that = this;
            that.saveAdditionalInfo();
        },
        tab4Cancel: function (e) {
            var that = this;
            if (that.consultantId == null) {
                that.set("consultantCollection", new STS.Portal.DataModels.RecruitementDataModel());
            } else {
                STS.Portal.DataSources.LoadConsultantDetail(that.consultantId, function (data) {
                    that.set("consultantCollection", data);
                });
            }
        },
        tab4Next: function (e) {
            var that = this;
            that.saveAdditionalInfo();
            var tabToActivate = $("#tab5li");
            $("#tabstrip").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);
        },
        tab4Previous: function () {
            //var that = this;
            //that.saveAdditionalInfo();
            //var tabToActivate = $("#tab3li");
            //$("#tabstrip").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);
        },
        saveAdditionalInfo: function () {
            var that = this;
            if (that.consultantCollection.primarySkillId == null ||
                that.consultantCollection.secondarySkillId == null ||
                that.consultantCollection.Salary_Amount == null ||
                that.consultantCollection.Salary_Type == null ||
                that.consultantCollection.SsnLastFive == null ||
                that.consultantCollection.Seeking_job_roles == null) {
                toastr.error("Please provide all the data before saving!");
                return false;
            }

            if ((that.consultantCollection.SsnLastFive).toString().length != 5) {
                toastr.error("Last five ssn is not proper!");
                return false;
            }

            that.consultantCollection.set("Context", "AdditionalInfo")
            STS.Portal.DataSources.UpdateConsultant(that.consultantCollection, function (data) {
                if (data.Statuscode == 200) {
                    that.set("consultantCollection", data.Consultant);
                    toastr.success(data.Status);
                    return true;
                } else {
                    toastr.error(data.Status);
                    return false;
                }
            });
        },
        addNewDocument: function () {
            var that = this;
            location.href = "#/addNewDocument/" + that.consultantId;
        },
        LoadDocumentTabData: function () {
            var that = this;
            STS.Portal.DataSources.getAllAuthTypes(function (result) {
                that.WorkAuthorizationTypeDs.data(result);
            });
            STS.Portal.DataSources.GetAllIdTypeAndPhotoTypes(function (result) {
                that.idTypeDs.data(result.idTypeList);
                that.photoIdTypeDs.data(result.photoTypeList);
            });
        },
        onDocumentSelect: function (e) {
            var that = this;
            //var allValidFiles = true;
            //$.each(e.files, function (index, value) {
            //    if (value.extension.toLowerCase() != ".png"
            //        && value.extension.toLowerCase() != ".jpg"
            //        && value.extension.toLowerCase() != ".jpeg"
            //        && value.extension.toLowerCase() != ".gif") {
            //        allValidFiles = false;
            //        alert("Only .png, .jpg, .jpeg and .gif files can be uploaded");
            //        e.preventDefault();
            //        return false;
            //    }
            //});
            //if (allValidFiles) {
            for (var i = 0; i < e.files.length; i++) {
                var fileReader = new FileReader();
                fileReader.onload = function (event) {
                    var mapImage = event.target.result;
                    that.documentCollection.set("Uploaded_Copy_base64", mapImage);
                    $("#clearPhotoHead").show();
                    $('.k-file').show();
                }
                fileReader.readAsDataURL(e.files[i].rawFile);
            }
            //}
        },
        addDocument: function (e) {
            var that = this;
            if (that.documentCollection.Work_Authorization_Type_ID == 0 ||
                that.documentCollection.ID_TYPE == 0 ||
                that.documentCollection.Photo_Type_ID == 0 ||
                that.documentCollection.Uploaded_Copy_base64 == "" ||
                that.documentCollection.Valid_From == "" ||
                that.documentCollection.Valid_To == "") {
                toastr.error("Please provide all the data before saving!");
                return false;
            }

            if (that.documentCollection.Valid_To < that.documentCollection.Valid_From) {
                toastr.error("Valid to should be greater than valid from!");
                return false;
            }

            that.documentCollection.set("Consultant_ID", that.consultantId);
            that.documentCollection.set("Work_Authorization_Type_Name", $("#workAuthorizationDropdownList-document").data("kendoDropDownList").text());
            that.documentCollection.set("ID_TYPE_Name", $("#idTypeDropdownList-document").data("kendoDropDownList").text());
            that.documentCollection.set("Photo_Type_ID_Name", $("#photoTypeDropdownList-document").data("kendoDropDownList").text());

            STS.Portal.DataSources.AddDocument(that.documentCollection, function (result) {
                that.set("documentCollection", new STS.Portal.DataModels.DocumentsDataModel());
                location.href = "#/Edit/" + that.consultantId;
            });
            for (var i = 0; i < result.length; i++) {
                result[i].Valid_From = new Date(result[i].Valid_From);
                result[i].Valid_To = new Date(result[i].Valid_To);
            }
            that.documentDs.data(result);
        },
        cancelDocument: function (e) {
            var that = this;
            that.set("documentCollection", new STS.Portal.DataModels.DocumentsDataModel());
            location.href = "#/Edit/" + that.consultantId;
        },
        onClickDownloadDocument: function (e) {
            var path = e.data.Uploaded_Copy;
            var ret = path.replace('.resource', '');
            window.location = ret;
        },
        editDocument: function (consultantId, proofId) {
            var that = this;
            that.LoadDocumentTabData();
            var data = that.documentDs.data();
            for (var i = 0; i < data.length; i++) {
                if (data[i].Proof_ID === proofId) {
                    that.set("documentCollection", data[i]);
                    console.log(that.documentCollection);
                }
            }
        },
        tab5Save: function () {
            var that = this;
            STS.Portal.DataSources.saveAllDocumentsByConsultantId(that.consultantId, function (result) {
                STS.Portal.DataSources.GetAllDocumentsByConsultantId(that.consultantId, function (result) {
                    for (var i = 0; i < result.length; i++) {
                        result[i].Valid_From = new Date(result[i].Valid_From);
                        result[i].Valid_To = new Date(result[i].Valid_To);
                    }
                    that.documentDs.data(result);
                    toastr.success("Document updated successfully!");
                });
            });
        },
        tab5Cancel: function () {
            var that = this;
            STS.Portal.DataSources.GetAllDocumentsByConsultantId(that.consultantId, function (result) {
                for (var i = 0; i < result.length; i++) {
                    result[i].Valid_From = new Date(result[i].Valid_From);
                    result[i].Valid_To = new Date(result[i].Valid_To);
                }
                that.documentDs.data(result);
            });
        },
        tab5Next: function () {
            var that = this;
            STS.Portal.DataSources.saveAllDocumentsByConsultantId(that.consultantId, function (result) {
                STS.Portal.DataSources.GetAllDocumentsByConsultantId(that.consultantId, function (result) {
                    for (var i = 0; i < result.length; i++) {
                        result[i].Valid_From = new Date(result[i].Valid_From);
                        result[i].Valid_To = new Date(result[i].Valid_To);
                    }
                    that.documentDs.data(result);
                    toastr.success("Document updated successfully!");
                    var tabToActivate = $("#tab6li");
                    $("#tabstrip").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);
                });
            });
        },
        tab5Previous: function () {
            //var that = this;
            //STS.Portal.DataSources.saveAllDocumentsByConsultantId(that.consultantId, function (result) {
            //    STS.Portal.DataSources.GetAllDocumentsByConsultantId(that.consultantId, function (result) {
            //        for (var i = 0; i < result.length; i++) {
            //            result[i].Valid_From = new Date(result[i].Valid_From);
            //            result[i].Valid_To = new Date(result[i].Valid_To);
            //        }
            //        that.documentDs.data(result);
            //        toastr.success("Document updated successfully!");
            //        var tabToActivate = $("#tab4li");
            //        $("#tabstrip").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);
            //    });
            //});
        },
        onClickDeleteEducation: function (e) {
            var that = this;
            STS.Portal.DataSources.DeleteDocument(e.data.Proof_ID, function () {
                STS.Portal.DataSources.GetAllDocumentsByConsultantId(that.consultantId, function (result) {
                    for (var i = 0; i < result.length; i++) {
                        result[i].Valid_From = new Date(result[i].Valid_From);
                        result[i].Valid_To = new Date(result[i].Valid_To);
                    }
                    that.documentDs.data(result);
                    toastr.success("Document updated successfully!");
                });
            });
        },
        tab6Save: function () {
            debugger;
            var that = this;
            if (that.consultantCollection.Status == null) {
                toastr.error("Please select status before saving!");
                return false;
            }

            that.consultantCollection.set("Context", "FinalSubmit");
            //STS.Portal.DataSources.UpdateConsultant(that.consultantCollection, function (data) {
            //    if (data.Statuscode === 200) {
            //        that.set("consultantCollection", data.Consultant);
            //        toastr.success(data.Status);
            //    } else {
            //        toastr.error(data.Status);
            //    }
            //});
            return false;
        },
        tab6Previous: function () {
            //var that = this;
            //if (that.consultantCollection.Status == null) {
            //    toastr.error("Please select status before saving!");
            //    return false;
            //}

            //that.consultantCollection.set("Context", "FinalSubmit");
            //STS.Portal.DataSources.UpdateConsultant(that.consultantCollection, function (data) {
            //    if (data.Statuscode === 200) {
            //        that.set("consultantCollection", data.Consultant);
            //        toastr.success(data.Status);

            //        var tabToActivate = $("#tab5li");
            //        $("#tabstrip").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);
            //    } else {
            //        toastr.error(data.Status);
            //    }
            //});
            //return false;
        },
    });

})(jQuery, window, document);
