﻿(function ($, window, document, undefined) {

    STS.Portal.vm.RecruitmentVM = function () {
        var allConsultantLayout = new kendo.View("AllConsultant-template", { model: STS.Portal.vm.RecruitmentViewModel });
        var addNewConsultantLayout = new kendo.View("AddNewConsultant-template", { model: STS.Portal.vm.RecruitmentViewModel });
        var addNewPrefferedLocationLayout = new kendo.View("AddPrefferedLocation-rowTemplate", { model: STS.Portal.vm.RecruitmentViewModel });
        var addNewEducationLayout = new kendo.View("AddNewEducation", { model: STS.Portal.vm.RecruitmentViewModel });
        var addNewDocumentLayout = new kendo.View("AddNewDocument", { model: STS.Portal.vm.RecruitmentViewModel });

        //#region ' Navigation Routings '
        var router = new kendo.Router({
            init: function () {
                STS.Portal.layouts.mainLayout.showIn(("#content-section"), allConsultantLayout);
                STS.Portal.vm.RecruitmentViewModel.set("IsInListMode", true);
                STS.Portal.vm.RecruitmentViewModel.set("IsInEditMode", false);
                STS.Portal.vm.RecruitmentViewModel.Load();
            }
        });

        router.route("/", function () {
            STS.Portal.layouts.mainLayout.showIn(("#content-section"), allConsultantLayout);
            STS.Portal.vm.RecruitmentViewModel.set("IsInListMode", true);
            STS.Portal.vm.RecruitmentViewModel.set("IsInEditMode", false);
        });

        router.route("/Add", function () {
            STS.Portal.layouts.mainLayout.showIn(("#content-section"), addNewConsultantLayout);
            STS.Portal.vm.RecruitmentViewModel.set("IsInListMode", false);
            STS.Portal.vm.RecruitmentViewModel.set("IsInEditMode", true);
            STS.Portal.vm.RecruitmentViewModel.LoadTabStrip();
        });

        router.route("/Edit/:ConsultantId", function (ConsultantId) {
            STS.Portal.layouts.mainLayout.showIn(("#content-section"), addNewConsultantLayout);
            STS.Portal.vm.RecruitmentViewModel.set("IsInListMode", false);
            STS.Portal.vm.RecruitmentViewModel.set("IsInEditMode", true);
            STS.Portal.vm.RecruitmentViewModel.set("consultantId", ConsultantId);
            STS.Portal.vm.RecruitmentViewModel.LoadTabStrip();
        });

        router.route("/AddPrefferedLocation/:ConsultantId", function (ConsultantId) {
            STS.Portal.layouts.mainLayout.showIn(("#content-section"), addNewPrefferedLocationLayout);
            STS.Portal.vm.RecruitmentViewModel.set("IsInListMode", false);
            STS.Portal.vm.RecruitmentViewModel.set("IsInEditMode", true);
            STS.Portal.vm.RecruitmentViewModel.set("consultantId", ConsultantId);
            STS.Portal.vm.RecruitmentViewModel.LoadAllDropdowns();
        });

        router.route("/addNewEducation/:ConsultantId", function (ConsultantId) {
            STS.Portal.layouts.mainLayout.showIn(("#content-section"), addNewEducationLayout);
            STS.Portal.vm.RecruitmentViewModel.set("IsInListMode", false);
            STS.Portal.vm.RecruitmentViewModel.set("IsInEditMode", true);
            STS.Portal.vm.RecruitmentViewModel.set("consultantId", ConsultantId);
            STS.Portal.vm.RecruitmentViewModel.LoadEducationData();
        });

        router.route("/addNewDocument/:ConsultantId", function (ConsultantId) {
            STS.Portal.layouts.mainLayout.showIn(("#content-section"), addNewDocumentLayout);
            STS.Portal.vm.RecruitmentViewModel.set("IsInListMode", false);
            STS.Portal.vm.RecruitmentViewModel.set("IsInEditMode", true);
            STS.Portal.vm.RecruitmentViewModel.set("consultantId", ConsultantId);
            STS.Portal.vm.RecruitmentViewModel.LoadDocumentTabData();
        });

        router.route("/EditPrefferedLocation/:ConsultantId/:prefLocId", function (ConsultantId, prefLocId) {
            STS.Portal.layouts.mainLayout.showIn(("#content-section"), addNewPrefferedLocationLayout);
            STS.Portal.vm.RecruitmentViewModel.set("IsInListMode", false);
            STS.Portal.vm.RecruitmentViewModel.set("IsInEditMode", true);
            STS.Portal.vm.RecruitmentViewModel.EditPreferredLoc(ConsultantId, prefLocId);
        });

        router.route("/editEducation/:ConsultantId/:Id", function (ConsultantId, Id) {
            STS.Portal.layouts.mainLayout.showIn(("#content-section"), addNewEducationLayout);
            STS.Portal.vm.RecruitmentViewModel.set("IsInListMode", false);
            STS.Portal.vm.RecruitmentViewModel.set("IsInEditMode", true);
            STS.Portal.vm.RecruitmentViewModel.editEducation(ConsultantId, Id);
        });

        router.route("/editDocument/:ConsultantId/:Proof_ID", function (ConsultantId, Proof_ID) {
            STS.Portal.layouts.mainLayout.showIn(("#content-section"), addNewDocumentLayout);
            STS.Portal.vm.RecruitmentViewModel.set("IsInListMode", false);
            STS.Portal.vm.RecruitmentViewModel.set("IsInEditMode", true);
            STS.Portal.vm.RecruitmentViewModel.editDocument(ConsultantId, Proof_ID);
        });
        
        return {
            start: function () {
                router.start();
            }
        };
    }
})(jQuery, window, document);