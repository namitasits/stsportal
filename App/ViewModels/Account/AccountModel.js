﻿(function ($, window, document, undefined) {
    STS.Portal.vm.accountModel = kendo.observable({
        accountCollection: new STS.Portal.DataModels.UsersDataModel(),
        userInfo: null,
        doLogIn: function (e) {
            var that = this;
            var data = that.get("accountCollection");

            var x = data.EmailId;
            if (x == "") {
                toastr.error("Enter e-mail address!");
                return false;
            }

            var atpos = x.indexOf("@");
            var dotpos = x.lastIndexOf(".");
            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                toastr.error("Not a valid e-mail address!");
                return false;
            }

            STS.Portal.DataSources.DoLogin(data, function (response) {
                if (response.Statuscode === 200) {
                    var token = response.token;
                    localStorage.setItem("token", token);
                    top.location.href = "/Dashboard/Home";
                } else {
                    alert(response.Status);
                }
            });
        }
    });
})(jQuery, window, document);