﻿(function ($, window, document, undefined) {

    STS.Portal.vm.AccountVM = function () {
        var loginLayout = new kendo.View("Login-template", { model: STS.Portal.vm.accountModel });

        var router = new kendo.Router({
            init: function () {
                STS.Portal.layouts.mainLayout.showIn(("#login-page"), loginLayout);
            }
        });

        router.route("/", function () {
        });

        return {
            start: function () {
                router.start();
            }
        };
    }
})(jQuery, window, document);
