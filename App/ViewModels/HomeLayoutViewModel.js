﻿STS.Portal.spa.HomeLayoutViewModel = function () {

    this.navigateToStep = function (route) {
        router.navigate(route);
    }


    var router = new kendo.Router({
        init: function () {
            var token = localStorage.getItem("token");
            if (token) {
                STS.Portal.vm.dashboardModel.loadview();
            }
        }
    });

    return {
        start: function () {
            router.start();
        }
    };
}