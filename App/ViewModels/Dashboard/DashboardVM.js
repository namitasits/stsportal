﻿(function ($, window, document, undefined) {

    STS.Portal.vm.DashboardVM = function () {
        var dashboardLayout = new kendo.View("dashboard-template", { model: STS.Portal.vm.dashboardModel });

        //#region ' Navigation Routings '
        var router = new kendo.Router({
            init: function () {
                STS.Portal.layouts.mainLayout.showIn(("#content-section"), dashboardLayout);
            }
        });

        router.route("/", function () {
        });

        return {
            start: function () {
                router.start();
            }
        };
    }
})(jQuery, window, document);