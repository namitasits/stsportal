﻿(function ($, window, document, undefined) {

    STS.Portal.vm.dashboardModel = kendo.observable({
        moduleName: "Dashboard",
        modulesDs: new kendo.data.DataSource({
            data: []
        }),
        loadview: function () {
            var that = this;
            STS.Portal.DataSources.getAllAccessibleModules(function (data) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].ModuleName === that.moduleName) {
                        //data[i].LiClass = "mt";
                        data[i].AClass = "active";
                    }
                }
                that.modulesDs.data(data);
            });

            $("#nav-accordion").kendoListView({
                dataSource: that.modulesDs,
                template: "<li class='#:LiClass#'><a class='#:AClass#' id='#:HtmlId#' href='#:HtmlHref#'><i class='#:HtmlClass#'></i><span>#:ModuleName#</span></a><li>"
            });
        },
        doLogout: function () {
            console.log(1);
        }
    });
})(jQuery, window, document);