﻿app_Key = "cf525291deea29d98f5be3f0bcb27257";
//https://battuta.medunes.net/

STS.Portal.DataSources.getAllAuthTypes = function (callBack) {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        url: "/api/Recruitement/" + "GetAllWorkAuthType",
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (e) {
            if (e.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
};

STS.Portal.DataSources.getAllCountires = function (callBack) {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        url: "/api/Recruitement/" + "GetAllCountires",
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (e) {
            if (e.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
};

STS.Portal.DataSources.GetAllStates = function (countryId, callBack) {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        url: "/api/Recruitement/" + "GetAllStatesByCountryId?countryId=" + countryId,
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (e) {
            if (e.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
};

STS.Portal.DataSources.GetAllCities = function (stateId, callBack) {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        url: "/api/Recruitement/" + "GetAllCitiesByStateId?stateId=" + stateId,
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (e) {
            if (e.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
};

STS.Portal.DataSources.GetAllUniversities = function (callBack) {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        url: "/api/Recruitement/" + "GetAllUniversities",
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (e) {
            if (e.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
};

STS.Portal.DataSources.GetAllDegrees = function (callBack) {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        url: "/api/Recruitement/" + "GetAllDegrees",
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (e) {
            if (e.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
};

STS.Portal.DataSources.CreateConsultant = function (consultantCollection, callBack) {
    $.ajax({
        url: "/api/Recruitement/" + "Save",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        type: "POST",
        data: JSON.stringify(consultantCollection),
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (error) {
            if (error.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
}

STS.Portal.DataSources.DeleteConsultant = function (consultantId, callBack) {
    $.ajax({
        url: "/api/Recruitement/" + "DeleteConsultant?consultantId=" + consultantId,
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        type: "GET",
        dataType: "JSON",
        success: callBack,
        error: function (error) {
            if (error.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
}

STS.Portal.DataSources.GetAllConsultants = function (callBack) {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        url: "/api/Recruitement/" + "GetAllConsultants",
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (e) {
            if (e.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
};

STS.Portal.DataSources.LoadConsultantDetail = function (consultantId, callBack) {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        url: "/api/Recruitement/" + "LoadConsultantDetail?id=" + consultantId,
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (e) {
            if (e.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
};

STS.Portal.DataSources.UpdateConsultant = function (consultantCollection, callBack) {
    $.ajax({
        url: "/api/Recruitement/" + "Update",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        type: "POST",
        data: JSON.stringify(consultantCollection),
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (error) {
            if (error.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
}

STS.Portal.DataSources.UpdateConsultantContactInformation = function (consultantCollection, callBack) {
    $.ajax({
        url: "/api/Recruitement/" + "UpdateConsultantContactInformation",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        type: "POST",
        data: JSON.stringify(consultantCollection),
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (error) {
            if (error.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
}

STS.Portal.DataSources.SavePreferredLocation = function (preferredLoacationCollection, callBack) {
    $.ajax({
        url: "/api/Recruitement/" + "SavePreferredLocation",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        type: "POST",
        data: JSON.stringify(preferredLoacationCollection),
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (error) {
            if (error.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
}

STS.Portal.DataSources.GetAllPreferredLocationByConsulantId = function (consultantId, callBack) {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        url: "/api/Recruitement/" + "GetAllPreferredLocationByConsulantId?consultantId=" + consultantId,
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (e) {
            if (e.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
};

STS.Portal.DataSources.GetPreferredLocForEdit = function (locId, callBack) {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        url: "/api/Recruitement/" + "GetPreferredLocForEdit?id=" + locId,
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (e) {
            if (e.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
};

STS.Portal.DataSources.DeletePreferredLocationByConsulantId = function (preferredLocation, callBack) {
    $.ajax({
        url: "/api/Recruitement/" + "DeletePreferredLocationByConsulantId",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        type: "POST",
        data: JSON.stringify(preferredLocation),
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (error) {
            if (error.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
}

STS.Portal.DataSources.AddUniversity = function (universityName, callBack) {
    $.ajax({
        url: "/api/Recruitement/" + "AddUniversity?university=" + universityName,
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        type: "GET",
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (error) {
            if (error.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
};

STS.Portal.DataSources.AddDegree = function (degreeName, callBack) {
    $.ajax({
        url: "/api/Recruitement/" + "AddDegree?degreeName=" + degreeName,
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        type: "GET",
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (error) {
            if (error.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
};

STS.Portal.DataSources.SaveEducationsList = function (education, callBack) {
    $.ajax({
        url: "/api/Recruitement/" + "SaveEducationsList",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        type: "POST",
        data: JSON.stringify(education),
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (error) {
            if (error.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
}

STS.Portal.DataSources.GetAllEducationDetailsByConsultantId = function (consultantId, callBack) {
    $.ajax({
        url: "/api/Recruitement/" + "GetAllEducationDetailsByConsultantId?consultantId=" + consultantId,
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        type: "GET",
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (error) {
            if (error.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
}

STS.Portal.DataSources.GetAllSkills = function (callBack) {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        url: "/api/Recruitement/" + "GetAllSkills",
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (e) {
            if (e.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
}

STS.Portal.DataSources.SaveAdditionalInformation = function (skillMapingCollection, callBack) {
    $.ajax({
        url: "/api/Recruitement/" + "SaveAdditionalInformation",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        type: "POST",
        data: JSON.stringify(skillMapingCollection),
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (error) {
            if (error.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
}

STS.Portal.DataSources.GetAllIdTypeAndPhotoTypes = function (callBack) {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        url: "/api/Recruitement/" + "GetAllIdTypeAndPhotoTypes",
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (e) {
            if (e.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
}

STS.Portal.DataSources.AddDocument = function (documentCollection, callBack) {
    $.ajax({
        url: "/api/Recruitement/" + "AddDocument",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        type: "POST",
        data: JSON.stringify(documentCollection),
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (error) {
            if (error.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
}

STS.Portal.DataSources.GetAllDocumentsByConsultantId = function (consultantId, callBack) {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        url: "/api/Recruitement/" + "GetAllDocumentsByConsultantId?consultantId=" + consultantId,
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (e) {
            if (e.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
}

STS.Portal.DataSources.saveAllDocumentsByConsultantId = function (consultantId, callBack) {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        url: "/api/Recruitement/" + "saveAllDocumentsByConsultantId?consultantId=" + consultantId,
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (e) {
            if (e.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
}

STS.Portal.DataSources.DeleteDocument = function (proofId, callBack) {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        url: "/api/Recruitement/" + "DeleteDocument?proofId=" + proofId,
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (e) {
            if (e.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
}

STS.Portal.DataSources.GetAllStatus = function (callBack) {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        url: "/api/Recruitement/" + "GetAllStatus",
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (e) {
            if (e.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
            }
        }
    });
}