﻿STS.Portal.DataSources.Operations = function () {

    function readDs(crudServiceBaseUrl, readEntity, readCallback) {
        var read = {
            url: crudServiceBaseUrl + readEntity,
            type: "GET",
            contentType: "application/json",
            dataType: "json",
            beforeSend: function (xhr) {
                var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
                if (jsonObj != null) {
                    var auth = "Bearer " + jsonObj.access_token;
                    xhr.setRequestHeader("Authorization", auth);
                }
                window.location.href = "/Account/Login";
            },
            complete: function () {
                if (readCallback) {
                    readCallback();
                }
            }
        };
        return read;
    }

    function createDs(crudServiceBaseUrl, saveEntity, successCallback) {
        var create = {
            async: false,
            url: crudServiceBaseUrl + saveEntity,
            type: "POST",
            contentType: "application/json",
            dataType: "json",
            beforeSend: function (xhr) {
                var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
                if (jsonObj != null) {
                    var auth = "Bearer " + jsonObj.access_token;
                    xhr.setRequestHeader("Authorization", auth);
                }
                window.location.href = "/Account/Login";
            },
            complete: function (jqXHR) {
                if (successCallback) {
                    successCallback(jqXHR);
                }
            }
        };
        return create;
    }

    function updateDs(crudServiceBaseUrl, saveEntity, successCallback) {
        var update = {
            async: false,
            url: crudServiceBaseUrl + saveEntity,
            type: "POST",
            contentType: "application/json",
            dataType: "json",
            beforeSend: function (xhr) {
                var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
                if (jsonObj != null) {
                    var auth = "Bearer " + jsonObj.access_token;
                    xhr.setRequestHeader("Authorization", auth);
                }
                window.location.href = "/Account/Login";
            },
            complete: function (jqXHR) {
                if (successCallback) {
                    if (jqXHR && jqXHR.responseText) {
                        successCallback(jqXHR);
                    }
                }
            }
        };
        return update;
    }

    function destroyDs(crudServiceBaseUrl, destroyEntity, successCallback) {
        var destroy = {
            async: false,
            url: crudServiceBaseUrl + destroyEntity,
            type: "POST",
            contentType: "application/json",
            dataType: "json",
            beforeSend: function (xhr) {
                var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
                if (jsonObj != null) {
                    var auth = "Bearer " + jsonObj.access_token;
                    xhr.setRequestHeader("Authorization", auth);
                }
                window.location.href = "/Account/Login";
            },
            complete: function (jqXHR) {
                if (successCallback) {
                    if (jqXHR && jqXHR.responseText) {
                        successCallback(jqXHR);
                    }
                }
            }
        };
        return destroy;
    }

    function errorDs(errorCallback, closeCallback) {
        var error = function (xhr) {
            var errorMes = JSON.parse(xhr.xhr.responseText);
            if (errorMes.Key) {
                common.showMessage(errorMes.Name, closeCallback);
                if (errorCallback) errorCallback();
            } else {
                common.showMessage("There was an error occurred!");
            }
        };
        return error;
    }

    return {
        readDs: readDs,
        createDs: createDs,
        updateDs: updateDs,
        destroyDs: destroyDs,
        errorDs: errorDs
    };
};
