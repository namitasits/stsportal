﻿var dataSourceOperations = new STS.Portal.DataSources.Operations();
var crudServiceBaseUrl = "/api/Dashboard/";

//STS.Portal.DataSources.getAllAccessibleModules = function (parameterMap, successCallback, readCallBack) {

//    return new kendo.data.DataSource({
//        transport: {
//            read: dataSourceOperations.readDs(crudServiceBaseUrl, "Get", readCallBack),
//            parameterMap: parameterMap
//        },
//        schema: {
//            //model: STS.Portal.DataModels.ModulesDataModel()
//        }
//    });

//};

STS.Portal.DataSources.getAllAccessibleModules = function (callBack) {
    $.ajax({
        type: "GET",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        url: crudServiceBaseUrl + "GetAllAccessibleModules",
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack,
        error: function (e) {
            if (e.status === 401) {
                localStorage.removeItem("token");
                window.location.href = "/Account/Login";
                //var obj = jQuery.parseJSON(localStorage.getItem("token"));
                //$.ajax({
                //    type: "GET",
                //    url: "/api/Account/" + "GenerateRefreshToken?refreshToken=" + obj.refresh_token,
                //    contentType: "application/JSON",
                //    dataType: "JSON",
                //    success: callBack,
                //})
            }
        }
    });
};
