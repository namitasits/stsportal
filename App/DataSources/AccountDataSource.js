﻿var crudServiceBaseUrl = "/api/Account/";
STS.Portal.DataSources.DoLogin = function (data, callBack) {
    $.ajax({
        type: "POST",
        url: crudServiceBaseUrl + "Login",
        data: JSON.stringify(data),
        contentType: "application/JSON",
        dataType: "JSON",
        success: callBack
    });
}

STS.Portal.DataSources.logoff = function (callBack) {
    $.ajax({
        type: "POST",
        url: crudServiceBaseUrl + "LogoffUser",
        beforeSend: function (xhr) {
            var jsonObj = jQuery.parseJSON(localStorage.getItem("token"));
            if (jsonObj == null) {
                window.location.href = "/Account/Login";
            }
            var auth = "Bearer " + jsonObj.access_token;
            xhr.setRequestHeader("Authorization", auth);
        },
        dataType: "JSON",
        contentType: "application/JSON",
        success: function (e) {
            if (e === "Success") {
                localStorage.clear();
                top.location.href = '/Account/Login?ReturnUrl=%2F';
            }
            return callBack;
        },
        error: function (error) {
        }
    });
}