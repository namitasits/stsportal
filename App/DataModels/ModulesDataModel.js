﻿STS.Portal.DataModels.ModulesDataModel = function () {
    return kendo.data.Model.define({
        id: "Module_Id",

        fields: {
            Module_Name: {
                type: "string",
                defaultValue: "",

                validation: {
                }
            },

            Html_id: {
                type: "string",
                defaultValue: "",

                validation: {
                }
            },

            Html_href: {
                type: "string",
                defaultValue: "",

                validation: {
                }
            },

            Html_Class: {
                type: "string",
                defaultValue: "",

                validation: {
                }
            },

            Li_Class: {
                type: "string",
                defaultValue: "",

                validation: {
                }
            },

            AClass: {
                type: "string",
                defaultValue: "",

                validation: {
                }
            }

        }
    });
}