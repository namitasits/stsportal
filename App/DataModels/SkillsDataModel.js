﻿STS.Portal.DataModels.SkillsDataModel = kendo.data.Model.define({
    id: "Skill_Id",

    fields: {
        Skill_Name: {
            type: "string",
        },
        Skill_Type: {
            type: "string",
        }
    }
});

 STS.Portal.DataModels.SkillMapingDataModel = kendo.data.Model.define({
     id: "Skill_Mapping_Id",

    fields: {
        Consultant_Id: {
            type: "string",
        },
        Skill_Id: {
            type: "int",
        }
    }
});
