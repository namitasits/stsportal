﻿STS.Portal.DataModels.EducationDataModel = kendo.data.Model.define({
    id: "University_Id",

    fields: {
        University_Name: {
            type: "string",
        },
        University_Address: {
            type: "string",
        },
        Degree_Id: {
            type:"int",
        },
        Degree_name: {
            type: "string",
        },
        StartDate: {
            type: "date",
            defaultValue: "",
        },
        EndDate: {
            type: "date",
            defaultValue: "",
        }
    }
});

STS.Portal.DataModels.DegreeModel = kendo.data.Model.define({
    id: "Degree_Id",

    fields: {
        Degree_name: {
            type: "string",
        },
        StartDate: {
            type: "date",
            defaultValue: ""
        },
        EndDate: {
            type: "date",
            defaultValue: ""
        }
    }
});
