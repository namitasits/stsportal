﻿STS.Portal.DataModels.RecruitementDataModel = kendo.data.Model.define({
    id: "ConsultantId",

    fields: {
        ConsultantDisplayId: {
            type: "string",
        },
        FName: {
            type: "string",
        },
        LName: {
            type: "string",
        },
        Email: {
            type: "string",
        },
        PhoneNumber: {
            type: "number",
            defaultValue: ""
        },
        WorkAuthorizationTypeId: {
            type: "number",
            defaultValue: ""
        },
        DoeUsa: {
            type: "date",
            defaultValue: ""
        },
        CurrentLocationCountry: {
            type: "string",
        },
        CurrentLocationCountryName: {
            type: "string",
        },
        CurrentLocationState: {
            type: "string",
        },
        CurrentLocationCity: {
            type: "string",
        },
        CurrentLocationStreet: {
            type: "string",
        },
        CurrentLocationZipcode: {
            type: "string",
        },
        Salary_Type: {
            type: "string",
        },
        Salary_Amount: {
            type: "string",
        },
        Seeking_job_roles: {
            type: "string",
        },
        Linkdein_Profile: {
            type: "string",
        },
        SsnLastFive: {
            type: "number",
            defaultValue: ""
        },
        ClientInterviewTimes: {
            type: "string",
        },
        VendorCallTimings: {
            type: "string",
        },
        Status: {
            type: "number",
            defaultValue: ""
        },
        CreatedBy: {
            type: "string",
        },
        UpdatedBy: {
            type: "string",
        },
        CreatedOn: {
            type: "date",
            defaultValue: "",
        },
        UpdatedOn: {
            type: "date",
            defaultValue: "",
        },

        //EXTRA
        Name: {
            type: "string",
            defaultValue: "",
        },
        Context: {
            type: "string",
            defaultValue: "",
        },
        Current_Location_Country_Id: {
            type: "int",
            defaultValue: "",
        },
        Current_Location_State_Id: {
            type: "int",
            defaultValue: "",
        },
        Current_Location_City_Id: {
            type: "int",
            defaultValue: "",
        },
        secondarySkillId: [],
        primarySkillId: []
    }
});
