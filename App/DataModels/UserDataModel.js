﻿STS.Portal.DataModels.UsersDataModel = kendo.data.Model.define({
    id: "App_UserId",

    fields: {
        UserName: {
            type: "string",
            defaultValue: "",

            validation: {
            }
        },

        EmailId: {
            type: "string",
            defaultValue: "",

            validation: {
            }
        },

        Name: {
            type: "string",
            defaultValue: "",

            validation: {
            }
        },

        Password: {
            type: "string",
            defaultValue: "",

            validation: {
            }
        }

    }
});