﻿STS.Portal.DataModels.PrefferedRelocationDataModel = kendo.data.Model.define({
    id: "Preferred_Relocation_Id",

    fields: {
        PreferredRelocationCountry: {
            type: "string",
        },
        PreferredRelocationCountryName: {
            type: "string",
        },
        PreferredRelocationState: {
            type: "string",
        },
        PreferredRelocationCity: {
            type: "string",
        },
        PreferredRelocationZipCode: {
            type: "string",
        },

        Preferred_Relocation_Country_Id: {
            type: "number",
        },
        Preferred_Relocation_State_Id: {
            type: "number",
        },
        Preferred_Relocation_City_Id: {
            type: "number",
        },
        ConsultantId: {
            type:"number",
        }
    }
});
