﻿STS.Portal.DataModels.DocumentsDataModel = kendo.data.Model.define({
    id: "Proof_ID",

    fields: {
        Consultant_ID: {
            type: "number"
        },
        Work_Authorization_Type_ID: {
            type: "number"
        },
        Work_Authorization_Type_Name: {
            type: "string"
        },
        ID_TYPE: {
            type: "number"
        },
        ID_TYPE_Name: {
            type: "string"
        },
        Photo_Type_ID: {
            type: "number"
        },
        Photo_Type_ID_Name: {
            type: "string"
        },
        Valid_From: {
            type: "date",
            defaultValue:""
        },
        Valid_To: {
            type: "date",
            defaultValue: ""
        },
        Status: {
            type: "boolean"
        },
        Uploaded_Copy: {
            type: "string"
        },
        Uploaded_Copy_base64: {
            tyep: "string"
        },
        Created_Date: {
            type: "date"
        },
        Created_Date: {
            type: "Updated_Date"
        }
    }
});
