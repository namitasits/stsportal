﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STS.Portal.DAL.Models
{
    public class Consultant
    {
        public int ConsultantId { get; set; }
        public string ConsultantDisplayId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int WorkAuthorizationTypeId { get; set; }
        public DateTime DoeUsa { get; set; }
        public string CurrentLocationCountry { get; set; }
        public string CurrentLocationCountryName { get; set; }
        public string CurrentLocationState { get; set; }
        public string CurrentLocationCity { get; set; }
        public string CurrentLocationStreet { get; set; }
        public string CurrentLocationZipcode { get; set; }
        public string SalaryType { get; set; }
        public string SalaryAmount { get; set; }
        public string SeekingJobRoles { get; set; }
        public string LinkdeinProfile { get; set; }
        public string SsnLastFive { get; set; }
        public string ClientInterviewTimes { get; set; }
        public string VendorCallTimings { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }

        //extra fields
        public string Name { get; set; }
        public string WorkAuthorizationTypeName { get; set; }
        public string Context { get; set; }
        public string[] SecondarySkillId { get; set; }
        public string[] PrimarySkillId { get; set; }
    }
}