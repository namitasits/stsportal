﻿using System.Data;

namespace STS.Portal.DAL.Models
{
    public partial class OauthClientDto
    {
        public string Id { get; set; }

        public string Secret { get; set; }

        public string Name { get; set; }

        public short? ApplicationType { get; set; }

        public bool? Active { get; set; }

        public int? RefreshTokenLifetime { get; set; }

        public string AllowedOrigin { get; set; }

        public int ClientId { get; set; }
    }
}