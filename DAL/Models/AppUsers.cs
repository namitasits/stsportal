﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace STS.Portal.DAL.Models
{
    public class AppUsers
    {
        public int AppUserId { get; set; }
        public string UserName { get; set; }
        public string EmailId { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public bool AccessToPortal { get; set; }
    }
}