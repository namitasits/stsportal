﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STS.Portal.DAL.Models
{
    public class University
    {
        public int UniversityId { get; set; }
        public string UniversityName { get; set; }
        public string UniversityAddress { get; set; }
    }

    public class Degree
    {
        public int DegreeId { get; set; }
        public string DegreeName { get; set; }
    }

    public class UniversityMapping
    {
        public int UniversityMappingId { get; set; }
        public int UniversityId { get; set; }
        public string UniversityName { get; set; }
        public int DegreeId { get; set; }
        public string DegreeName { get; set; }
        public int ConsultantId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

    }
}