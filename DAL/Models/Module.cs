﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STS.Portal.DAL.Models
{
    public class Module
    {
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public string HtmlId { get; set; }
        public string HtmlHref { get; set; }
        public string HtmlClass { get; set; }
        public string LiClass { get; set; }
        public string AClass { get; set; }
    }
}