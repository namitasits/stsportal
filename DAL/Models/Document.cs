﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STS.Portal.DAL.Models
{
    public class Document
    {
        public int ProofId { get; set; }
        public int ConsultantId { get; set; }
        public int WorkAuthorizationTypeId { get; set; }
        public string WorkAuthorizationTypeName { get; set; }
        public int IdType { get; set; }
        public string IdTypeName { get; set; }
        public int PhotoTypeId { get; set; }
        public string PhotoTypeIdName { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public Boolean Status { get; set; }
        public string UploadedCopy { get; set; }
        public string UploadedCopyBase64 { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

    }

    public class IdType
    {
        public int IdTypeId { get; set; }
        public string IdTypeName { get; set; }
    }

    public class PhotoTypeId
    {
        public int Photo_Type_Id { get; set; }
        public string PhotoTypeName { get; set; }
    }
}