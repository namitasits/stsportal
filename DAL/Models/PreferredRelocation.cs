﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STS.Portal.DAL.Models
{
    public class PreferredRelocation
    {
        public int PreferredRelocationId { get; set; }
        public string PreferredRelocationCountry { get; set; }
        public string PreferredRelocationState { get; set; }
        public string PreferredRelocationCity { get; set; }
        public string PreferredRelocationZipCode { get; set; }
        public int ConsultantId { get; set; }

        public int PreferredRelocationCountryId { get; set; }
        public int PreferredRelocationStateId { get; set; }
        public int PreferredRelocationCityId { get; set; }
    }
}