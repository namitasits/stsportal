﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STS.Portal.DAL.Models
{
    public class WorkAuthorizationType
    {
        public int WorkAuthorizationTypeId { get; set; }
        public string WorkAuthorizationTypeName { get; set; }
    }

    public class Status
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }
}