﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STS.Portal.DAL.Models
{
    public class Skill
    {
        public int SkillId { get; set; }
        public string SkillName { get; set; }
        public string SkillType { get; set; }
    }

    public class SkillMapping
    {
        public int SkillMappingId { get; set; }
        public int SkillId { get; set; }
        public int ConsultantId { get; set; }
        public string SkillType { get; set; }
    }
}