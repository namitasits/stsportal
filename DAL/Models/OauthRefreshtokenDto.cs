﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STS.Portal.DAL.Models
{
    public class OauthRefreshtokenDto
    {
        public string Id { get; set; }

        public string Subject { get; set; }

        public string ClientId { get; set; }

        public DateTimeOffset? IssuedUtc { get; set; }

        public DateTimeOffset? ExpiresUtc { get; set; }

        public string ProtectedTicket { get; set; }

        public int RefreshtokenId { get; set; }
    }
}