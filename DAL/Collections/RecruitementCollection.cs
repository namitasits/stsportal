﻿using STS.Portal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace STS.Portal.DAL.Collections
{
    public struct Constants
    {
        public const string EncryptionExtension = ".resource";
    }

    public class RecruitementCollection
    {
        private readonly string _strConnString = ConfigurationManager.ConnectionStrings["HRPORTALV1ConnectionString"]
            .ConnectionString;

        public List<WorkAuthorizationType> GetAllWorkAuthType()
        {
            var authTypeList = new List<WorkAuthorizationType>();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_All_WorkAuthorization_Type", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var authType = new WorkAuthorizationType
                        {
                            WorkAuthorizationTypeId = (int)reader["Work_Authorization_Type_ID"],
                            WorkAuthorizationTypeName = reader["Work_Authorization_Type_Name"].ToString()
                        };
                        authTypeList.Add(authType);
                    }
                }
            }
            return authTypeList;
        }
        public List<Countries> GetAllCountires()
        {
            var list = new List<Countries>();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_All_Countries", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var countires = new Countries
                        {
                            Id = Convert.ToInt32(reader["id"]),
                            Name = Convert.ToString(reader["name"])
                        };
                        list.Add(countires);
                    }
                }
            }
            return list;
        }
        public List<States> GetAllStatesByCountryId(int countryId)
        {
            var list = new List<States>();
            using (var con = new SqlConnection(_strConnString))
            {
                if (countryId != 0)
                {
                    using (var cmd = new SqlCommand("Load_States_By_Country_Id", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@country_id", countryId);
                        con.Open();

                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            var state = new States
                            {
                                Id = Convert.ToInt32(reader["id"]),
                                Name = Convert.ToString(reader["name"])
                            };
                            list.Add(state);
                        }
                    }
                }
                else
                {
                    using (var cmd = new SqlCommand("Load_All_States", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        con.Open();

                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            var state = new States();
                            state.Id = Convert.ToInt32(reader["id"]);
                            state.Name = Convert.ToString(reader["name"]);
                            list.Add(state);
                        }
                    }
                }
            }
            return list;
        }
        public List<City> GetAllCitiesByStateId(int stateId)
        {
            var list = new List<City>();
            using (var con = new SqlConnection(_strConnString))
            {
                if (stateId != 0)
                {
                    using (var cmd = new SqlCommand("Load_Cities_By_State_Id", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@state_id", stateId);
                        con.Open();

                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            var city = new City
                            {
                                Id = Convert.ToInt32(reader["id"]),
                                Name = Convert.ToString(reader["name"])
                            };
                            list.Add(city);
                        }
                    }
                }
                else
                {
                    using (var cmd = new SqlCommand("Load_All_Cities", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        con.Open();

                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            var city = new City
                            {
                                Id = Convert.ToInt32(reader["id"]),
                                Name = Convert.ToString(reader["name"])
                            };
                            list.Add(city);
                        }
                    }
                }
            }
            return list;
        }
        public List<University> GetAllUniversities()
        {
            var list = new List<University>();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_All_Universities", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var university = new University
                        {
                            UniversityId = Convert.ToInt32(reader["University_Id"]),
                            UniversityName = Convert.ToString(reader["University_Name"]),
                            UniversityAddress = Convert.ToString(reader["University_Address"])
                        };
                        list.Add(university);
                    }
                }
            }
            return list;
        }
        public List<University> AddUniversity(string university)
        {
            var list = new List<University>();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Insert_University", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@University_Name", university);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var uni = new University
                        {
                            UniversityId = Convert.ToInt32(reader["University_Id"]),
                            UniversityName = Convert.ToString(reader["University_Name"]),
                            UniversityAddress = Convert.ToString(reader["University_Address"])
                        };
                        list.Add(uni);
                    }
                }
            }
            return list;
        }
        public List<Degree> GetAllDegrees()
        {
            var list = new List<Degree>();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_All_Degrees", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var degree = new Degree
                        {
                            DegreeId = Convert.ToInt32(reader["Degree_Id"]),
                            DegreeName = Convert.ToString(reader["Degree_name"])
                        };
                        list.Add(degree);
                    }
                }
            }
            return list;
        }
        public List<Degree> AddDegree(string degreeName)
        {
            var list = new List<Degree>();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Insert_Degree", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Degree_name", degreeName);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var degree = new Degree
                        {
                            DegreeId = Convert.ToInt32(reader["Degree_Id"]),
                            DegreeName = Convert.ToString(reader["Degree_name"])
                        };
                        list.Add(degree);
                    }
                }
            }
            return list;
        }
        public List<Skill> GetAllSkills()
        {
            var list = new List<Skill>();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_All_Skills", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var skill = new Skill
                        {
                            SkillId = Convert.ToInt32(reader["Skill_Id"]),
                            SkillName = Convert.ToString(reader["Skill_Name"]),
                            SkillType = Convert.ToString(reader["Skill_Type"])
                        };
                        list.Add(skill);
                    }
                }
            }
            return list;
        }
        public List<IdType> GetAllIdType()
        {
            var list = new List<IdType>();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_All_ID_TYPE", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var idType = new IdType
                        {
                            IdTypeId = Convert.ToInt32(reader["ID_Type_ID"]),
                            IdTypeName = Convert.ToString(reader["ID_Type_Name"])
                        };
                        list.Add(idType);

                    }
                    con.Close();
                }
            }
            return list;
        }
        public List<PhotoTypeId> GetAllPhotoIdType()
        {
            var list = new List<PhotoTypeId>();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_All_Photo_Type_ID", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var photoIdType = new PhotoTypeId
                        {
                            Photo_Type_Id = Convert.ToInt32(reader["Photo_Type_ID"]),
                            PhotoTypeName = Convert.ToString(reader["Photo_Type_Name"])
                        };
                        list.Add(photoIdType);

                    }
                    con.Close();
                }
            }
            return list;
        }
        public List<Status> GetAllStatus()
        {
            var list = new List<Status>();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_All_Status", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var status = new Status();
                        status.StatusId = Convert.ToInt32(reader["Status_Id"]);
                        status.StatusName = Convert.ToString(reader["Status"]);
                        list.Add(status);
                    }
                }
            }
            return list;
        }


        public int CreateConsultant(Consultant consultant)
        {
            var sameEmail = CheckIfSameEmailIdExists(consultant.Email);
            if (sameEmail) return 0;
            var consultantId = 0;
            var consultantDisplayId = CreateConsultantDisplayId(consultant);
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Create_consultant_Profile", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Consultant_Display_Id", consultantDisplayId);
                    cmd.Parameters.AddWithValue("@fName", consultant.FName);
                    cmd.Parameters.AddWithValue("@lName", consultant.LName);
                    cmd.Parameters.AddWithValue("@Email", consultant.Email);
                    cmd.Parameters.AddWithValue("@PhoneNumber", consultant.PhoneNumber);
                    cmd.Parameters.AddWithValue("@Work_Authorization_Type", consultant.WorkAuthorizationTypeId);
                    cmd.Parameters.AddWithValue("@doeUSA", consultant.DoeUsa);
                    cmd.Parameters.AddWithValue("@CreatedBy", consultant.CreatedBy);
                    cmd.Parameters.AddWithValue("@UpdatedBy", null);
                    cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                    cmd.Parameters.AddWithValue("@UpdatedOn", null);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        consultantId = Convert.ToInt32(reader["Consultant_Id"]);
                    }
                }
            }
            return consultantId;
        }

        public string CreateConsultantDisplayId(Consultant consultant)
        {
            string fName = (consultant.FName).Substring(0, 2);
            string lName = (consultant.LName).Substring(0, 2);
            string day = (consultant.DoeUsa.Day).ToString();
            string month = (consultant.DoeUsa.Month).ToString();
            string year = (consultant.DoeUsa.Year).ToString();

            string[] numberArray = new string[year.Length];
            int counter = 0;

            for (int i = 0; i < year.Length; i++)
            {
                numberArray[i] = year.Substring(counter, 1); // 1 is split length
                counter++;
            }

            if (day.Length < 2) day = "0" + day;
            if (month.Length < 2) month = "0" + month;

            return fName + lName + day + month + numberArray[2] + numberArray[3];
        }

        public bool CheckIfSameEmailIdExists(string emailId)
        {
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Check_If_Same_Email_Id_Exists", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@EmailId", emailId);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    if (!reader.HasRows) return false;
                    return true;
                }
            }
        }

        public List<Consultant> GetAllConsultants(int userId)
        {
            var consultantList = new List<Consultant>();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_All_Consultants", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@User_Id", userId);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var consultant = new Consultant
                        {
                            ConsultantId = Convert.ToInt32(reader["Consultant_Id"]),
                            ConsultantDisplayId = Convert.ToString(reader["Consultant_Display_Id"]),
                            Name = Convert.ToString(reader["fName"]) + " " + Convert.ToString(reader["lName"]),
                            Email = Convert.ToString(reader["Email"]),
                            PhoneNumber = Convert.ToString(reader["PhoneNumber"]),
                            Status = Convert.ToString(reader["Status"])
                        };
                        consultantList.Add(consultant);
                    }
                }
            }
            return consultantList;
        }

        public Consultant LoadConsultantDetailById(int consultantId)
        {
            var consultant = new Consultant();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_Consultant_Details_By_Id", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Consultant_Id", consultantId);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        consultant = this.FillConsultantObjectFromReader(reader);
                    }
                    con.Close();
                }

                var pList = new List<SkillMapping>();
                var sList = new List<SkillMapping>();
                using (var cmd = new SqlCommand("Get_Skill_Mapping_By_Consultant_Id", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Consultant_Id", consultantId);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var skillMap = new SkillMapping
                        {
                            SkillMappingId = Convert.ToInt32(reader["Skill_Mapping_Id"]),
                            SkillId = Convert.ToInt32(reader["Skill_Id"]),
                            SkillType = Convert.ToString(reader["Skill_Type"])
                        };

                        if (skillMap.SkillType == "Primary") pList.Add(skillMap);
                        else sList.Add(skillMap);
                    }
                    con.Close();
                }

                var primarySkillId = new string[pList.Count];
                var secondarySkillId = new string[sList.Count];

                for (var i = 0; i < primarySkillId.Length; i++)
                {
                    primarySkillId[i] = pList[i].SkillId.ToString();
                }

                for (var i = 0; i < secondarySkillId.Length; i++)
                {
                    secondarySkillId[i] = sList[i].SkillId.ToString();
                }

                consultant.PrimarySkillId = primarySkillId;
                consultant.SecondarySkillId = secondarySkillId;
            }
            return consultant;
        }

        public Consultant UpdatePersonalInfo(Consultant consultant)
        {
            var updatedConsultant = new Consultant();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Update_Consultant_Profile_Info", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Consultant_Id", consultant.ConsultantId);
                    cmd.Parameters.AddWithValue("@fName", consultant.FName);
                    cmd.Parameters.AddWithValue("@lName", consultant.LName);
                    cmd.Parameters.AddWithValue("@Email", consultant.Email);
                    cmd.Parameters.AddWithValue("@doeUSA", consultant.DoeUsa);
                    cmd.Parameters.AddWithValue("@PhoneNumber", consultant.PhoneNumber);
                    cmd.Parameters.AddWithValue("@Work_Authorization_Type", consultant.WorkAuthorizationTypeId);
                    cmd.Parameters.AddWithValue("@UpdatedBy", consultant.UpdatedBy);
                    cmd.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    if (!reader.HasRows) return null;
                    while (reader.Read())
                    {
                        updatedConsultant = FillConsultantObjectFromReader(reader);
                    }
                }
            }
            return updatedConsultant;
        }

        public Consultant UpdateConsultantContactInformation(Consultant consultant)
        {
            var updatedConsultant = new Consultant();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Update_Consultant_Contact_Information", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Consultant_Id", consultant.ConsultantId);
                    cmd.Parameters.AddWithValue("@Current_Location_Country", consultant.CurrentLocationCountryName);
                    cmd.Parameters.AddWithValue("@Current_Location_State", consultant.CurrentLocationState);
                    cmd.Parameters.AddWithValue("@Current_Location_City", consultant.CurrentLocationCity);
                    cmd.Parameters.AddWithValue("@Current_Location_Street", consultant.CurrentLocationStreet);
                    cmd.Parameters.AddWithValue("@Current_Location_Zipcode", consultant.CurrentLocationZipcode);
                    cmd.Parameters.AddWithValue("@UpdatedBy", consultant.UpdatedBy);
                    cmd.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    if (!reader.HasRows) return null;
                    while (reader.Read())
                    {
                        updatedConsultant = FillConsultantObjectFromReader(reader);
                    }
                }
            }
            return updatedConsultant;
        }

        public int SavePreferredLocation(PreferredRelocation preRelocation)
        {
            var id = 0;
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Create_Preferred_Relocation", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Preferred_Relocation_Country_Id", preRelocation.PreferredRelocationCountryId);
                    cmd.Parameters.AddWithValue("@Preferred_Relocation_Country", preRelocation.PreferredRelocationCountry);
                    cmd.Parameters.AddWithValue("@Preferred_Relocation_State_Id", preRelocation.PreferredRelocationStateId);
                    cmd.Parameters.AddWithValue("@Preferred_Relocation_State", preRelocation.PreferredRelocationState);
                    cmd.Parameters.AddWithValue("@Preferred_Relocation_City_id", preRelocation.PreferredRelocationCityId);
                    cmd.Parameters.AddWithValue("@Preferred_Relocation_City", preRelocation.PreferredRelocationCity);
                    cmd.Parameters.AddWithValue("@Preferred_Relocation_ZipCode", preRelocation.PreferredRelocationZipCode);
                    cmd.Parameters.AddWithValue("@Consultant_Id", preRelocation.ConsultantId);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        id = Convert.ToInt32(reader["Preferred_Relocation_Id"]);
                    }
                }
            }
            return id;
        }

        public void UpdatePreferredLocation(PreferredRelocation preRelocation)
        {
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Update_Preferred_Location", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Preferred_Relocation_Id", preRelocation.PreferredRelocationId);
                    cmd.Parameters.AddWithValue("@Preferred_Relocation_Country_Id", preRelocation.PreferredRelocationCountryId);
                    cmd.Parameters.AddWithValue("@Preferred_Relocation_Country", preRelocation.PreferredRelocationCountry);
                    cmd.Parameters.AddWithValue("@Preferred_Relocation_State_Id", preRelocation.PreferredRelocationStateId);
                    cmd.Parameters.AddWithValue("@Preferred_Relocation_State", preRelocation.PreferredRelocationState);
                    cmd.Parameters.AddWithValue("@Preferred_Relocation_City_Id", preRelocation.PreferredRelocationCityId);
                    cmd.Parameters.AddWithValue("@Preferred_Relocation_City", preRelocation.PreferredRelocationCity);
                    cmd.Parameters.AddWithValue("@Preferred_Relocation_ZipCode", preRelocation.PreferredRelocationZipCode);
                    con.Open();

                    cmd.ExecuteReader();
                }
            }
        }

        public List<PreferredRelocation> GetAllPreferredLocationByConsulantId(int consultantId)
        {
            var list = new List<PreferredRelocation>();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_All_Preferred_Location_By_Consulant_Id", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Consultant_Id", consultantId);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var preLoc =
                            new PreferredRelocation
                            {
                                PreferredRelocationId = Convert.ToInt32(reader["Preferred_Relocation_Id"]),
                                PreferredRelocationCountry = Convert.ToString(reader["Preferred_Relocation_Country"]),
                                PreferredRelocationState = Convert.ToString(reader["Preferred_Relocation_State"]),
                                PreferredRelocationCity = Convert.ToString(reader["Preferred_Relocation_City"]),
                                PreferredRelocationZipCode = Convert.ToString(reader["Preferred_Relocation_ZipCode"]),
                                ConsultantId = consultantId
                            };
                        list.Add(preLoc);
                    }
                }
            }
            return list;
        }

        public PreferredRelocation GetPreferredLocForEdit(int id)
        {
            var loc = new PreferredRelocation();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_Preferred_Loc_For_Edit", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", id);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        loc.PreferredRelocationId = Convert.ToInt32(reader["Preferred_Relocation_Id"]);
                        loc.PreferredRelocationCountry = Convert.ToString(reader["Preferred_Relocation_Country"]);
                        loc.PreferredRelocationState = Convert.ToString(reader["Preferred_Relocation_State"]);
                        loc.PreferredRelocationCity = Convert.ToString(reader["Preferred_Relocation_City"]);
                        loc.PreferredRelocationZipCode = Convert.ToString(reader["Preferred_Relocation_ZipCode"]);
                        loc.PreferredRelocationCountryId = Convert.ToInt32(reader["Preferred_Relocation_Country_Id"]);
                        loc.PreferredRelocationStateId = Convert.ToInt32(reader["Preferred_Relocation_State_Id"]);
                        loc.PreferredRelocationCityId = Convert.ToInt32(reader["Preferred_Relocation_City_Id"]);
                    }
                }
            }
            return loc;
        }

        public void DeletePreferredLocationByConsulantId(int consultantId)
        {
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Delete_Preferred_Location_by_Consultant_Id", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", consultantId);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }

        public void SaveEducationsList(List<UniversityMapping> universityMapping)
        {
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Delete_Education_Details_By_Consultant_Id", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Consultant_Id", universityMapping[0].ConsultantId);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }

                foreach (var education in universityMapping)
                {
                    using (var cmd = new SqlCommand("Add_Education_Details", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@University_Id", education.UniversityId);
                        cmd.Parameters.AddWithValue("@University_Name", education.UniversityName);
                        cmd.Parameters.AddWithValue("@Degree_Id", education.DegreeId);
                        cmd.Parameters.AddWithValue("@Degree_Name", education.DegreeName);
                        cmd.Parameters.AddWithValue("@Consultant_Id", education.ConsultantId);
                        cmd.Parameters.AddWithValue("@StartDate", education.StartDate);
                        cmd.Parameters.AddWithValue("@EndDate", education.EndDate);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
        }

        public List<UniversityMapping> GetAllEducationDetailsByConsultantId(int consultantId)
        {
            var list = new List<UniversityMapping>();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_All_Education_Details_By_Consultant_Id", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Consultant_Id", consultantId);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var education =
                            new UniversityMapping
                            {
                                UniversityMappingId = Convert.ToInt32(reader["University_Mapping_Id"]),
                                UniversityId = Convert.ToInt32(reader["University_Id"]),
                                UniversityName = Convert.ToString(reader["University_Name"]),
                                DegreeId = Convert.ToInt32(reader["Degree_Id"]),
                                DegreeName = Convert.ToString(reader["Degree_Name"]),
                                StartDate = Convert.ToDateTime(reader["StartDate"]),
                                EndDate = Convert.ToDateTime(reader["EndDate"]),
                                ConsultantId = Convert.ToInt32(reader["Consultant_Id"])
                            };
                        list.Add(education);
                    }
                }
            }
            return list;
        }

        public Consultant UpdateAdditionalInformation(Consultant consultant)
        {
            var updatedConsultant = new Consultant();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Update_Additional_Information_By_Consultant_Id", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Salary_Type", consultant.SalaryType);
                    cmd.Parameters.AddWithValue("@Salary_Amount", consultant.SalaryAmount);
                    cmd.Parameters.AddWithValue("@Seeking_job_roles", consultant.SeekingJobRoles);
                    cmd.Parameters.AddWithValue("@ssnLastFive", consultant.SsnLastFive);
                    cmd.Parameters.AddWithValue("@VendorCallTimings", consultant.VendorCallTimings);
                    cmd.Parameters.AddWithValue("@ClientInterviewTimes", consultant.ClientInterviewTimes);
                    cmd.Parameters.AddWithValue("@Linkdein_Profile", consultant.LinkdeinProfile);
                    cmd.Parameters.AddWithValue("@Consultant_Id", consultant.ConsultantId);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        updatedConsultant = FillConsultantObjectFromReader(reader);
                    }
                }
            }
            return updatedConsultant;
        }

        public Consultant UpdateStatus(Consultant consultant)
        {
            var updatedConsultant = new Consultant();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Update_Status", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Status", consultant.Status);
                    cmd.Parameters.AddWithValue("@Consultant_Id", consultant.ConsultantId);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        updatedConsultant = FillConsultantObjectFromReader(reader);
                    }
                }
            }
            return updatedConsultant;
        }

        public void AddUpdateSkillMapping(Consultant consultant)
        {
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Delete_Skill_Mappings_By_Consultant_Id", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Consultant_Id", consultant.ConsultantId);
                    con.Open();

                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

            var primarySkillIds = consultant.PrimarySkillId;
            var secondarySkillIds = consultant.SecondarySkillId;

            var list = primarySkillIds.Select(t => new SkillMapping
                {
                    SkillId = Convert.ToInt32(t),
                    SkillType = "Primary",
                    ConsultantId = consultant.ConsultantId
                })
                .ToList();
            list.AddRange(secondarySkillIds.Select(t => new SkillMapping
            {
                SkillId = Convert.ToInt32(t),
                SkillType = "Secondary",
                ConsultantId = consultant.ConsultantId
            }));

            using (var con = new SqlConnection(_strConnString))
            {
                foreach (var skillMapping in list)
                {
                    using (var cmd = new SqlCommand("Add_Skill_Mappings", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Skill_Id", skillMapping.SkillId);
                        cmd.Parameters.AddWithValue("@Consultant_Id", skillMapping.ConsultantId);
                        cmd.Parameters.AddWithValue("@Skill_Type", skillMapping.SkillType);
                        con.Open();

                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
        }

        public List<Document> AddDocument(Document document)
        {
            var path = GetDocumentPath(document);
            var list = new List<Document>();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Insert_Document_By_ConsultantId", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Consultant_ID", document.ConsultantId);
                    cmd.Parameters.AddWithValue("@Work_Authorization_Type_ID", document.WorkAuthorizationTypeId);
                    cmd.Parameters.AddWithValue("@Work_Authorization_Type_ID_Name", document.WorkAuthorizationTypeName);
                    cmd.Parameters.AddWithValue("@ID_TYPE", document.IdType);
                    cmd.Parameters.AddWithValue("@TD_TYPE_Name", document.IdTypeName);
                    cmd.Parameters.AddWithValue("@Photo_Type_ID", document.PhotoTypeId);
                    cmd.Parameters.AddWithValue("@Photo_Type_ID_Name", document.PhotoTypeIdName);
                    cmd.Parameters.AddWithValue("@Valid_From", document.ValidFrom);
                    cmd.Parameters.AddWithValue("@Valid_To", document.ValidTo);
                    cmd.Parameters.AddWithValue("@Status", document.Status);
                    cmd.Parameters.AddWithValue("@Uploaded_Copy", path);
                    cmd.Parameters.AddWithValue("@Created_Date", DateTime.Now);
                    cmd.Parameters.AddWithValue("@temp_Save", true);
                    cmd.Parameters.AddWithValue("@Uploaded_Copy_base64", "");
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var doc = new Document
                        {
                            ProofId = Convert.ToInt32(reader["Proof_ID"]),
                            ConsultantId = Convert.ToInt32(reader["Consultant_ID"]),
                            WorkAuthorizationTypeId = Convert.ToInt32(reader["Work_Authorization_Type_ID"]),
                            WorkAuthorizationTypeName = Convert.ToString(reader["Work_Authorization_Type_ID_Name"]),
                            IdType = Convert.ToInt32(reader["ID_TYPE"]),
                            IdTypeName = Convert.ToString(reader["TD_TYPE_Name"]),
                            PhotoTypeId = Convert.ToInt32(reader["Photo_Type_ID"]),
                            PhotoTypeIdName = Convert.ToString(reader["Photo_Type_ID_Name"]),
                            ValidFrom = Convert.ToDateTime(reader["Valid_From"]),
                            ValidTo = Convert.ToDateTime(reader["Valid_To"]),
                            Status = Convert.ToBoolean(reader["Status"]),
                            UploadedCopy = Convert.ToString(reader["Uploaded_Copy"])
                        };

                        GC.Collect();
                        GC.WaitForPendingFinalizers();

                        doc.UploadedCopyBase64 = Convert.ToString(Convert.ToBase64String(File.ReadAllBytes(doc.UploadedCopy)));
                        list.Add(doc);
                    }
                    con.Close();
                }
            }
            return list;
        }

        public string GetDocumentPath(Document document)
        {
            var count = 1;
            var subPath = "~/Consultants/" + "Documents" + "/" + document.ConsultantId + "/";

            if (String.IsNullOrEmpty(document.UploadedCopyBase64)) return null;
            var fileName = GetFileName(document.ConsultantId, document.UploadedCopyBase64,
                document.WorkAuthorizationTypeName, document.IdTypeName, document.PhotoTypeIdName);

            var filePath = HttpContext.Current.Server.MapPath(subPath + fileName);
            var folderExists = Directory.Exists(HttpContext.Current.Server.MapPath(subPath));
            if (!folderExists)
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(subPath));
            }

            while (File.Exists(filePath))
            {
                var tempFileName = document.ConsultantId + "_" + document.WorkAuthorizationTypeName + "_" +
                                   document.IdTypeName + "_" + document.PhotoTypeIdName + count++ + "." +
                                   document.UploadedCopyBase64.Split(';')[0].Split('/')[1];
                filePath = HttpContext.Current.Server.MapPath(subPath + tempFileName);
            }

            File.Create(filePath);
            return filePath;
        }

        public string GetFileName(int consultantId, string base64, string workAuthorizationTypeIdName, string idTypeName, string photoTypeIdNamee)
        {
            return consultantId + "_" + workAuthorizationTypeIdName + "_" + idTypeName + "_" + photoTypeIdNamee + "." + ((base64.Split(';'))[0].Split('/'))[1];
        }

        public List<Document> GetAllDocumentsByConsultantId(int consultantId)
        {
            var list = new List<Document>();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_All_Documents_By_Consultant_Id", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Consultant_ID", consultantId);
                    cmd.Parameters.AddWithValue("@temp_Save", false);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var doc = new Document
                        {
                            ProofId = Convert.ToInt32(reader["Proof_ID"]),
                            ConsultantId = Convert.ToInt32(reader["Consultant_ID"]),
                            WorkAuthorizationTypeId = Convert.ToInt32(reader["Work_Authorization_Type_ID"]),
                            WorkAuthorizationTypeName = Convert.ToString(reader["Work_Authorization_Type_ID_Name"]),
                            IdType = Convert.ToInt32(reader["ID_TYPE"]),
                            IdTypeName = Convert.ToString(reader["TD_TYPE_Name"]),
                            PhotoTypeId = Convert.ToInt32(reader["Photo_Type_ID"]),
                            PhotoTypeIdName = Convert.ToString(reader["Photo_Type_ID_Name"]),
                            ValidFrom = Convert.ToDateTime(reader["Valid_From"]),
                            ValidTo = Convert.ToDateTime(reader["Valid_To"]),
                            Status = Convert.ToBoolean(reader["Status"]),
                            UploadedCopy = Convert.ToString(reader["Uploaded_Copy"]),
                            UploadedCopyBase64 = Convert.ToString(reader["Uploaded_Copy_base64"])
                        };
                        list.Add(doc);
                    }
                }
            }
            return list;
        }

        public void SaveAllDocumentsByConsultantId(int consultantId)
        {
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Save_All_Documents_By_Consultant_Id", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Consultant_ID", consultantId);
                    cmd.Parameters.AddWithValue("@temp_Save", false);
                    con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteDocument(int proofId)
        {
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Delete_Document", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@proofId", proofId);
                    con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public Consultant FillConsultantObjectFromReader(SqlDataReader reader)
        {
            var consultant = new Consultant
            {
                ConsultantId = Convert.ToInt32(reader["Consultant_Id"]),
                ConsultantDisplayId = Convert.ToString(reader["Consultant_Display_Id"]),
                FName = Convert.ToString(reader["fName"]),
                LName = Convert.ToString(reader["lName"]),
                Email = Convert.ToString(reader["Email"]),
                PhoneNumber = Convert.ToString(reader["PhoneNumber"])
            };

            if (!(reader["Work_Authorization_Type"] is DBNull))
                consultant.WorkAuthorizationTypeId = Convert.ToInt32(reader["Work_Authorization_Type"]);

            consultant.DoeUsa = Convert.ToDateTime(reader["doeUSA"]);

            if (!(reader["Current_Location_Country"] is DBNull))
                consultant.CurrentLocationCountryName = Convert.ToString(reader["Current_Location_Country"]);

            if (!(reader["Current_Location_State"] is DBNull))
                consultant.CurrentLocationState = Convert.ToString(reader["Current_Location_State"]);

            if (!(reader["Current_Location_City"] is DBNull))
                consultant.CurrentLocationCity = Convert.ToString(reader["Current_Location_City"]);

            consultant.CurrentLocationStreet = Convert.ToString(reader["Current_Location_Street"]);
            consultant.CurrentLocationZipcode = Convert.ToString(reader["Current_Location_Zipcode"]);
            consultant.SalaryType = Convert.ToString(reader["Salary_Type"]);
            consultant.SalaryAmount = Convert.ToString(reader["Salary_Amount"]);
            consultant.SeekingJobRoles = Convert.ToString(reader["Seeking_job_roles"]);
            consultant.SsnLastFive = Convert.ToString(reader["SsnLastFive"]);
            consultant.VendorCallTimings = Convert.ToString(reader["VendorCallTimings"]);
            consultant.ClientInterviewTimes = Convert.ToString(reader["ClientInterviewTimes"]);
            consultant.LinkdeinProfile = Convert.ToString(reader["Linkdein_Profile"]);
            consultant.Status = Convert.ToString(reader["Status"]);
            return consultant;
        }

        public void DeleteConsultant(int consultantId)
        {
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Delete_Consultant" , con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@consultantId" , consultantId);
                    con.Open();

                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}