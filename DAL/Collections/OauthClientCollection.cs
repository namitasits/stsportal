﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using STS.Portal.DAL.Models;

namespace STS.Portal.DAL.Collections
{
    public class OauthClientCollection
    {
        private readonly string _strConnString = ConfigurationManager.ConnectionStrings["HRPORTALV1ConnectionString"]
            .ConnectionString;

        public OauthClientDto GetByClientId(string clientId)
        {
            var oauthClientDto = new OauthClientDto();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_By_Client_Id", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", clientId);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        oauthClientDto.Id = reader["Id"].ToString();
                        oauthClientDto.Secret = reader["Secret"].ToString();
                        oauthClientDto.Name = reader["Name"].ToString();
                        oauthClientDto.ApplicationType = reader["ApplicationType"] as short?;
                        oauthClientDto.Active = reader["Active"] as bool?;
                        oauthClientDto.RefreshTokenLifetime = reader["RefreshTokenLifetime"] as int?;
                        oauthClientDto.AllowedOrigin = reader["AllowedOrigin"].ToString();
                        oauthClientDto.ClientId = (int) reader["ClientId"];
                    }
                }
            }
            return oauthClientDto;
        }
    }
}