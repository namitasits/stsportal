﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using STS.Portal.DAL.Models;

namespace STS.Portal.DAL.Collections
{
    public class AccountCollection
    {
        private readonly string _strConnString = ConfigurationManager.ConnectionStrings["HRPORTALV1ConnectionString"]
            .ConnectionString;

        public AppUsers DoLogin(string emailId, string password)
        {
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("app_login", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@EmailId", SqlDbType.VarChar).Value = emailId;
                    cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = password;
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var appUsers = new AppUsers
                        {
                            AppUserId = (int)reader["App_UserId"],
                            EmailId = (string)reader["EmailId"],
                            UserName = (string)reader["UserName"],
                            Name = (string)reader["Name"],
                        };

                        if (!(reader["Access_To_Portal"] is DBNull))
                            appUsers.AccessToPortal = Convert.ToBoolean(reader["Access_To_Portal"]);
                        return appUsers;
                    }
                }
            }
            return null;
        }

        public string[] GetUserRoles(int appUserId)
        {
            var roleIDs = GetRoleIdsByUserId(appUserId);
            var roleNames = GetRoleNamesByRoleId(roleIDs);

            return roleNames;
        }

        public string GetRoleIdsByUserId(int appUserId)
        {
            var count = 0;
            var roleIds = new int[count];
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_Role_Ids_By_UserId", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@User_Id", SqlDbType.Int).Value = appUserId;
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    var dt = new DataTable();
                    dt.Load(reader);

                    count = dt.Rows.Count;
                    Array.Resize(ref roleIds, count);

                    int i;
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        roleIds[i] = (int)dt.Rows[i]["Role_Id"];
                    }
                    con.Close();
                }

                var roleIDs = string.Join(",", roleIds);
                return roleIDs;
            }
        }

        public string[] GetRoleNamesByRoleId(string roleIDs)
        {
            var count = 0;
            var roleNames = new string[count];
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_Role_Names_By_Role_Id", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Role_Ids", SqlDbType.VarChar).Value = roleIDs;
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    var dt = new DataTable();
                    dt.Load(reader);
                    con.Close();

                    count = dt.Rows.Count;
                    Array.Resize(ref roleNames, count);

                    int i;
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        roleNames[i] = dt.Rows[i]["Role_Name"].ToString();
                    }
                    con.Close();
                }
            }
            return roleNames;
        }

        public AppUsers GetByUserName(string emailId)
        {
            var appUsers = new AppUsers();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_User_By_UserName", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@EmailId", emailId);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        appUsers.AppUserId = (int) reader["App_UserId"];
                        appUsers.UserName = reader["UserName"].ToString();
                        appUsers.EmailId = reader["EmailId"].ToString();
                        appUsers.Name = reader["Name"].ToString();
                        appUsers.AccessToPortal = (bool) reader["Access_To_Portal"];
                        appUsers.Password = reader["Password"].ToString();
                    }
                }
            }
            return appUsers;
        }
    }
}