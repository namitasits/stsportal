﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using STS.Portal.DAL.Models;

namespace STS.Portal.DAL.Collections
{
    public class OauthRefreshtokenCollection
    {
        private readonly string _strConnString = ConfigurationManager.ConnectionStrings["HRPORTALV1ConnectionString"]
            .ConnectionString;

        private void RemoveRefreshToken(OauthRefreshtokenDto dto)
        {
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Remove_Refresh_Token", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@RefreshtokenId", dto.RefreshtokenId);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }

        public OauthRefreshtokenDto GetById(string hashedTokenId)
        {
            var oauthRefreshTokenDto = new OauthRefreshtokenDto();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("oauth_Refresh_token_Get_By_Id", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@hashedTokenId", hashedTokenId);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        oauthRefreshTokenDto.Id = reader["Id"].ToString();
                        oauthRefreshTokenDto.Subject = reader["Subject"].ToString();
                        oauthRefreshTokenDto.ClientId = reader["ClientId"].ToString();
                        oauthRefreshTokenDto.IssuedUtc = reader["IssuedUtc"] as DateTimeOffset?;
                        oauthRefreshTokenDto.ExpiresUtc = reader["ExpiresUtc"] as DateTimeOffset?;
                        oauthRefreshTokenDto.ProtectedTicket = reader["ProtectedTicket"].ToString();
                        oauthRefreshTokenDto.RefreshtokenId = (int)reader["RefreshtokenId"];
                    }
                }
            }
            return oauthRefreshTokenDto;
        }

        public bool RemoveByHashedTokendId(string hashedTokenId)
        {
            OauthRefreshtokenDto refreshtoken = GetById(hashedTokenId);
            RemoveRefreshToken(refreshtoken);
            return true;
        }

        private OauthRefreshtokenDto GetTokenBySubjectAndClientId(string subject, string clientId)
        {
            var oauthRefreshTokenDto = new OauthRefreshtokenDto();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_Token_By_Subject_And_ClientId", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Subject", subject);
                    cmd.Parameters.AddWithValue("@ClientId", clientId);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        oauthRefreshTokenDto.Id = reader["Id"].ToString();
                        oauthRefreshTokenDto.Subject = reader["Subject"].ToString();
                        oauthRefreshTokenDto.ClientId = reader["ClientId"].ToString();
                        oauthRefreshTokenDto.IssuedUtc = reader["IssuedUtc"] as DateTimeOffset?;
                        oauthRefreshTokenDto.ExpiresUtc = reader["ExpiresUtc"] as DateTimeOffset?;
                        oauthRefreshTokenDto.ProtectedTicket = reader["ProtectedTicket"].ToString();
                        oauthRefreshTokenDto.RefreshtokenId = (int)reader["RefreshtokenId"];
                    }
                }
            }
            return oauthRefreshTokenDto;
        }

        public bool AddRefreshToken(OauthRefreshtokenDto dto)
        {
            var existingToken = GetTokenBySubjectAndClientId(dto.Subject, dto.ClientId);

            if (existingToken != null)
            {
                RemoveRefreshToken(dto);
            }

            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Add_Refresh_Token", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", dto.Id);
                    cmd.Parameters.AddWithValue("@Subject", dto.Subject);
                    cmd.Parameters.AddWithValue("@ClientId", dto.ClientId);
                    cmd.Parameters.AddWithValue("@IssuedUtc", dto.IssuedUtc);
                    cmd.Parameters.AddWithValue("@ExpiresUtc", dto.ExpiresUtc);
                    cmd.Parameters.AddWithValue("@ProtectedTicket", dto.ProtectedTicket);

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}