﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using STS.Portal.DAL.Models;

namespace STS.Portal.DAL.Collections
{
    public class DashboardCollection
    {
        readonly string _strConnString = ConfigurationManager.ConnectionStrings["HRPORTALV1ConnectionString"]
            .ConnectionString;

        public List<Module> GetAllAccessibleModuleByUserId(int userId)
        {
            var moduleList = new List<Module>();
            using (var con = new SqlConnection(_strConnString))
            {
                using (var cmd = new SqlCommand("Get_All_Accessible_Modules_By_UserId", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserId", userId);
                    con.Open();

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var module = new Module
                        {
                            ModuleId = (int) reader["Module_Id"],
                            ModuleName = reader["Module_Name"].ToString(),
                            HtmlId = reader["Html_id"].ToString(),
                            HtmlHref = reader["Html_href"].ToString(),
                            HtmlClass = reader["Html_Class"].ToString(),
                            AClass = reader["a_Class"].ToString(),
                            LiClass = reader["Li_Class"] == null ? "" : reader["Li_Class"].ToString()
                        };
                        moduleList.Add(module);
                    }
                }
            }
            return moduleList;
        }
    }
}