﻿//Creates a gloabl object called templateLoader with a single method "loadExtTemplate"
var viewFolderBasePath = "Templates/";

var templateLoader = (function (jQuery, host) {

    function checkLastArgumentIsOnCompleteFunction(args) {
        return (args[args.length - 1] instanceof Function);
    }

    return {
        //Method: loadExtTemplate
        //Params: (string) path: the relative path to a file that contains template definition(s)
        loadExtTemplate: function () {
            var argCount = arguments.length;
            if (argCount < 2) return;
            if (!checkLastArgumentIsOnCompleteFunction(arguments)) return;

            var onComplete = arguments[argCount - 1];
            argCount--; //this is reduce loop not to consider onComplate as Path

            var promiseArray = new Array();

            for (var i = 0; i < argCount; i++) {
                promiseArray.push(
                  jQuery.get(viewFolderBasePath + arguments[i], function (html) {
                      $("body").append(html);
                  })
                );
            }

            try {
                jQuery.when.apply(jQuery, promiseArray).done(onComplete);
            } catch (e) {

            }

        }
    };

})(jQuery, document);