﻿using Newtonsoft.Json;
using STS.Portal.DAL.Collections;
using STS.Portal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace STS.Portal.Controllers.API
{
    public class RecruitementController : BaseApiController
    {
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAllWorkAuthType()
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                var workAuthType = recruitementCollection.GetAllWorkAuthType();
                return Request.CreateResponse(HttpStatusCode.OK , workAuthType);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage Save(Consultant consultant)
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                consultant.CreatedBy = UserId.ToString();
                var id = recruitementCollection.CreateConsultant(consultant);

                var recruitementCollectionForLoad = new RecruitementCollection();
                var cons = recruitementCollectionForLoad.LoadConsultantDetailById(id);

                if (id == 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest , new
                    {
                        Statuscode = 400 ,
                        Status = "EmailId already exists!"
                    });
                }
                return Request.CreateResponse(HttpStatusCode.OK , new
                {
                    consultant = cons ,
                    Statuscode = 200 ,
                    Status = "Created Successfully!"
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAllConsultants()
        {
            try
            {
                var recuitementCollection = new RecruitementCollection();
                var consultants = recuitementCollection.GetAllConsultants(UserId);
                return Request.CreateResponse(HttpStatusCode.OK , consultants);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage LoadConsultantDetail(int id)
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                var consultant = recruitementCollection.LoadConsultantDetailById(id);
                return Request.CreateResponse(HttpStatusCode.OK , consultant);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage Update(Consultant consultant)
        {
            if (consultant == null) return Request.CreateResponse(HttpStatusCode.BadRequest , "Null consultant!");
            try
            {
                consultant.UpdatedBy = UserId.ToString();
                if (consultant.Context == "PersonalInfo")
                {
                    var recCollectionForPersonalInfo = new RecruitementCollection();
                    var updatedPersonalInfo = recCollectionForPersonalInfo.UpdatePersonalInfo(consultant);

                    return Request.CreateResponse(HttpStatusCode.OK , new
                    {
                        Statuscode = 200 ,
                        Consultant = updatedPersonalInfo ,
                        Status = "Updated Successfully!"
                    });
                }
                if (consultant.Context == "ContactInformation")
                {
                    var recCollectionForContactInfo = new RecruitementCollection();
                    var updatedPersonalInfo = recCollectionForContactInfo.UpdateConsultantContactInformation(consultant);

                    return Request.CreateResponse(HttpStatusCode.OK , new
                    {
                        Statuscode = 200 ,
                        Consultant = updatedPersonalInfo ,
                        Status = "Updated Successfully!"
                    });
                }
                if (consultant.Context == "AdditionalInfo")
                {
                    var recCollectionForUpdatingConsultant = new RecruitementCollection();
                    var updatedPersonalInfo = recCollectionForUpdatingConsultant.UpdateAdditionalInformation(consultant);

                    var recCollectionForUpdatingSkillMapping = new RecruitementCollection();
                    recCollectionForUpdatingSkillMapping.AddUpdateSkillMapping(consultant);
                    updatedPersonalInfo.PrimarySkillId = consultant.PrimarySkillId;
                    updatedPersonalInfo.SecondarySkillId = consultant.SecondarySkillId;

                    return Request.CreateResponse(HttpStatusCode.OK , new
                    {
                        Statuscode = 200 ,
                        Consultant = updatedPersonalInfo ,
                        Status = "Updated Successfully!"
                    });
                }
                if (consultant.Context == "FinalSubmit")
                {
                    var recCollectionForUpdatingConsultant = new RecruitementCollection();
                    var updatedStatus = recCollectionForUpdatingConsultant.UpdateStatus(consultant);

                    return Request.CreateResponse(HttpStatusCode.OK , new
                    {
                        Statuscode = 200 ,
                        Consultant = updatedStatus ,
                        Status = "Updated Successfully!"
                    });
                }
                return Request.CreateResponse(HttpStatusCode.OK , consultant);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAllCountires()
        {
            try
            {
                var recCollection = new RecruitementCollection();
                var countries = recCollection.GetAllCountires();
                return Request.CreateResponse(HttpStatusCode.OK , countries);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAllStatesByCountryId(int countryId)
        {
            try
            {
                var recCollection = new RecruitementCollection();
                var states = recCollection.GetAllStatesByCountryId(countryId);
                return Request.CreateResponse(HttpStatusCode.OK , states);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAllCitiesByStateId(int stateId)
        {
            try
            {
                var recCollection = new RecruitementCollection();
                var states = recCollection.GetAllCitiesByStateId(stateId);
                return Request.CreateResponse(HttpStatusCode.OK , states);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage SavePreferredLocation(PreferredRelocation preRelocation)
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                if (preRelocation.PreferredRelocationId == 0)
                {
                    var id = recruitementCollection.SavePreferredLocation(preRelocation);
                    return Request.CreateResponse(HttpStatusCode.OK , new
                    {
                        PreferredRelocation_Id = id ,
                        Statuscode = 200 ,
                        Status = "Preferred Location saved!"
                    });
                }
                else
                {
                    recruitementCollection.UpdatePreferredLocation(preRelocation);
                    var id = preRelocation.PreferredRelocationId;
                    return Request.CreateResponse(HttpStatusCode.OK , new
                    {
                        PreferredRelocation_Id = id ,
                        Statuscode = 200 ,
                        Status = "Preferred Location updated!"
                    });
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAllPreferredLocationByConsulantId(int consultantId)
        {
            try
            {
                var recCollection = new RecruitementCollection();
                var locations = recCollection.GetAllPreferredLocationByConsulantId(consultantId);
                return Request.CreateResponse(HttpStatusCode.OK , locations);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetPreferredLocForEdit(int id)
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                var consultant = recruitementCollection.GetPreferredLocForEdit(id);
                return Request.CreateResponse(HttpStatusCode.OK , consultant);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage DeletePreferredLocationByConsulantId(PreferredRelocation preRelocation)
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                recruitementCollection.DeletePreferredLocationByConsulantId(preRelocation.PreferredRelocationId);
                return Request.CreateResponse(HttpStatusCode.OK , "Preferred Location Deleted!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAllUniversities()
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                var universities = recruitementCollection.GetAllUniversities();
                return Request.CreateResponse(HttpStatusCode.OK , universities);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage AddUniversity(string university)
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                var uni = recruitementCollection.AddUniversity(university);
                return Request.CreateResponse(HttpStatusCode.OK , uni);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAllDegrees()
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                var degrees = recruitementCollection.GetAllDegrees();
                return Request.CreateResponse(HttpStatusCode.OK , degrees);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage AddDegree(string degreeName)
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                var degree = recruitementCollection.AddDegree(degreeName);
                return Request.CreateResponse(HttpStatusCode.OK , degree);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Authorize]
        public void SaveEducationsList(List<UniversityMapping> educationList)
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                recruitementCollection.SaveEducationsList(educationList);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public List<UniversityMapping> GetAllEducationDetailsByConsultantId(int consultantId)
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                var list = recruitementCollection.GetAllEducationDetailsByConsultantId(consultantId);
                return list;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAllSkills()
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                var skills = recruitementCollection.GetAllSkills();

                List<Skill> primarySkills = new List<Skill>();
                List<Skill> secondarySkills = new List<Skill>();

                foreach (var skill in skills)
                {
                    if (skill.SkillType == "Primary")
                    {
                        primarySkills.Add(skill);
                    }
                    else
                    {
                        secondarySkills.Add(skill);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK , new
                {
                    primarySkills ,
                    secondarySkills
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAllIdTypeAndPhotoTypes()
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                var idTypeList = recruitementCollection.GetAllIdType();

                var recruitementCollectionForPhotoIdType = new RecruitementCollection();
                var photoTypeList = recruitementCollectionForPhotoIdType.GetAllPhotoIdType();

                return Request.CreateResponse(HttpStatusCode.OK , new
                {
                    idTypeList ,
                    photoTypeList
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage AddDocument(Document document)
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                var list = recruitementCollection.AddDocument(document);

                return Request.CreateResponse(HttpStatusCode.OK , list);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAllDocumentsByConsultantId(int consultantId)
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                var list = recruitementCollection.GetAllDocumentsByConsultantId(consultantId);

                return Request.CreateResponse(HttpStatusCode.OK , list);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage SaveAllDocumentsByConsultantId(int consultantId)
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                recruitementCollection.SaveAllDocumentsByConsultantId(consultantId);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage DeleteDocument(int proofId)
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                recruitementCollection.DeleteDocument(proofId);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAllStatus()
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                var status = recruitementCollection.GetAllStatus();

                return Request.CreateResponse(HttpStatusCode.OK , status);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage DeleteConsultant(int consultantId)
        {
            try
            {
                var recruitementCollection = new RecruitementCollection();
                recruitementCollection.DeleteConsultant(consultantId);

                var recuitementCollectionForGetAll = new RecruitementCollection();
                var consultants = recuitementCollectionForGetAll.GetAllConsultants(UserId);
                return Request.CreateResponse(HttpStatusCode.OK , consultants);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
