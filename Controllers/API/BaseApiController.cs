﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;

namespace STS.Portal.Controllers.API
{
    public class BaseApiController : ApiController
    {
        public int UserId
        {
            get { return string.IsNullOrEmpty(HttpContext.Current.User.Identity.GetUserId()) ? 0 : Convert.ToInt32(HttpContext.Current.User.Identity.GetUserId()); }
        }
    }
}
