﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.ModelBinding;
using System.Web.Security;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using STS.Portal.DAL;
using STS.Portal.DAL.Collections;
using STS.Portal.DAL.Models;

namespace STS.Portal.Controllers.API
{
    public class AccountController : ApiController
    {

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return this.Request.GetOwinContext().Authentication;
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public HttpResponseMessage Login(AppUsers appUser)
        {
            try
            {
                if (string.IsNullOrEmpty(appUser.EmailId) || string.IsNullOrEmpty(appUser.Password))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }

                //Do Login 
                var accountCollection = new AccountCollection();
                var user = accountCollection.DoLogin(appUser.EmailId, appUser.Password);
                if (user != null)
                {
                    // Get user roles by userId
                    var accountCollectionForUserInfo = new AccountCollection();
                    var userRoles = accountCollectionForUserInfo.GetUserRoles(user.AppUserId);
                    var roles = string.Join(",", userRoles);

                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                    var identity = CreateAsync(user, roles);
                    AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = true }, identity);

                    var accessToken = GenerateAccessToken(user.EmailId, appUser.Password);

                    var loginSuccessResponse = new
                    {
                        Userid = user.AppUserId,
                        Username = user.UserName,
                        Emailid = user.EmailId,
                        Roles = userRoles,
                        token = accessToken,
                        AccessToPortal = true,
                        Status = "Login Successfully!",
                        Statuscode = HttpStatusCode.OK
                    };

                    return Request.CreateResponse(HttpStatusCode.OK, loginSuccessResponse);
                }

                var loginFailedResponse = new
                {
                    Status = "Login failed!",
                    Statuscode = HttpStatusCode.Unauthorized
                };
                return Request.CreateResponse(HttpStatusCode.OK, loginFailedResponse);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public string GenerateAccessToken(string emailId, string password)
        {
            var accessTokenUrl = System.Configuration.ConfigurationManager.AppSettings["AccessTokenUrl"];
            var tRequest = (HttpWebRequest)WebRequest.Create(accessTokenUrl);

            var postData = "grant_type=password";
            postData += "&username=" + emailId;
            postData += "&password=" + password;
            postData += "&client_id=" + ConfigurationManager.AppSettings["client_id"];
            postData += "&client_secret=" + ConfigurationManager.AppSettings["client_secret"];

            var data = Encoding.ASCII.GetBytes(postData);

            tRequest.Method = "POST";
            tRequest.ContentType = "application/x-www-form-urlencoded";
            tRequest.ContentLength = data.Length;

            using (var stream = tRequest.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)tRequest.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return responseString;
        }

        private ClaimsIdentity CreateAsync(AppUsers user, string userRoles)
        {
            // This is a standard place where you can register your ClaimsIdentityFactory.
            // This class generates a ClaimsIdentity for the given user
            // By default it adds Roles, UserName, UserId as Claims for the User
            // You can add more standard set of claims here if you want to.
            // The Following sample shows how you can add more claims in a centralized way.
            // This sample does not add Roles as Claims

            var id = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie, ClaimTypes.NameIdentifier, null);
            id.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Name, ClaimValueTypes.String));
            id.AddClaim(new Claim(ClaimTypes.Anonymous, user.AccessToPortal.ToString(), ClaimValueTypes.Boolean));

            id.AddClaim(new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName, ClaimValueTypes.String));
            id.AddClaim(new Claim(ClaimsIdentity.DefaultRoleClaimType, userRoles, ClaimValueTypes.String));

            const string IdentityProviderClaimType = "http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider";
            const string DefaultIdentityProviderClaimValue = "ASP.NET Identity";
            id.AddClaim(new Claim(IdentityProviderClaimType, DefaultIdentityProviderClaimValue, ClaimValueTypes.String));

            // Add the claims for the login time
            id.AddClaim(new Claim("LoginTime", System.DateTime.Now.ToString()));
            return id;
        }

        [HttpGet]
        public HttpResponseMessage GenerateRefreshToken(string refreshToken)
        {
            if (string.IsNullOrEmpty(refreshToken))
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            var responseString = GenerateNewAccessToken(refreshToken);
            return Request.CreateResponse(HttpStatusCode.OK, responseString);
        }

        private string GenerateNewAccessToken(string refreshToken)
        {
            var tRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["AccessTokenUrl"]);

            var postData = "grant_type=refresh_token";
            postData += "&refresh_token=" + refreshToken;
            postData += "&client_id=" + ConfigurationManager.AppSettings["client_id"];
            postData += "&client_secret=" + ConfigurationManager.AppSettings["client_secret"];

            var data = System.Text.Encoding.ASCII.GetBytes(postData);

            tRequest.Method = "POST";
            tRequest.ContentType = "application/x-www-form-urlencoded";
            tRequest.ContentLength = data.Length;

            using (var stream = tRequest.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)tRequest.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return responseString;
        }

        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage LogoffUser()
        {
            try
            {
                HttpContext.Current.GetOwinContext().Authentication.SignOut(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie);
                return Request.CreateResponse(HttpStatusCode.OK , "Success");
            }
            catch (Exception ex)
            {
                throw ex;
                return Request.CreateResponse(HttpStatusCode.OK , "error");
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }

}

