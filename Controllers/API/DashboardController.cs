﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using STS.Portal.DAL.Collections;

namespace STS.Portal.Controllers.API
{
    public class DashboardController : BaseApiController
    {
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAllAccessibleModules()
        {
            try
            {
                var dashboardCollection = new DashboardCollection();
                var module = dashboardCollection.GetAllAccessibleModuleByUserId(UserId);
                return Request.CreateResponse(HttpStatusCode.OK , module);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
